package jp.bap.api.designpattern.Adapter.vd2;

/**
 * SquarePegs không tương thích với RoundHoles (chúng được triển khai bởi
 * nhóm phát triển trước đó). Nhưng chúng tôi phải tích hợp chúng vào chương trình của chúng tôi.
 */
public class SquarePeg {
    private double width;

    public SquarePeg(double width) {
        this.width = width;
    }

    public double getWidth() {
        return width;
    }

    public double getSquare() {
        double result;
        result = Math.pow(this.width, 2);
        return result;
    }
}
