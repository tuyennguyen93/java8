package jp.bap.api.designpattern.Adapter.vd1;

// Target: đây là nội dung message được Client cung cấp cho thông dịch viên (Translator / Adapter).
public interface VietnameseTarget {
    void send(String words);
}
