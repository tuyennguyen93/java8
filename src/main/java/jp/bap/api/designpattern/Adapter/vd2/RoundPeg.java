package jp.bap.api.designpattern.Adapter.vd2;

// RoundPegs(Chốt tròn) tương thích với RoundHoles(Lỗ tròn).
public class RoundPeg {
    private double radius;

    public RoundPeg() {}

    public RoundPeg(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }
}
