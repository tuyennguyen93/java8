package jp.bap.api.designpattern.Adapter.vd2;

public class Demo {
    public static void main(String[] args) {
        // Vòng vừa vặn, không có gì bất ngờ.
        RoundHole hole = new RoundHole(5);
        RoundPeg rpeg = new RoundPeg(5);
        if (hole.fits(rpeg)) {
            System.out.println("Vòng tròn r5 vừa vặn với lỗ tròn r5.");
        }

        SquarePeg smallSqPeg = new SquarePeg(2);
        SquarePeg largeSqPeg = new SquarePeg(20);
         //hole.fits(smallSqPeg); // gây lỗi.

        // Adaptor giải quyết vấn đề.
        SquarePegAdapter smallSqPegAdapter = new SquarePegAdapter(smallSqPeg);
        SquarePegAdapter largeSqPegAdapter = new SquarePegAdapter(largeSqPeg);
        if (hole.fits(smallSqPegAdapter)) {
            System.out.println("Chốt vuông w2 vừa vặn với lỗ tròn r5.");
        }
        if (!hole.fits(largeSqPegAdapter)) {
            System.out.println("Chốt vuông w20 không vừa với lỗ tròn r5.");
        }
    }
}
