package jp.bap.api.designpattern.Singleton;

public class NormalClass {
    public String value;
    public NormalClass(String value) {
        System.out.println("gọi hàm khởi tạo cho NormalClass");
        this.value = value;
    }

}
