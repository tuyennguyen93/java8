package jp.bap.api.designpattern.Singleton;

public class DemoSingleThread {
    public static void main(String[] args) {
        System.out.println("use Singleton" + "\n");
        // lần thứ 1 nó gọi hàm khởi tạo
        Singleton singleton = Singleton.getInstance("FOO");
        System.out.println(singleton);
        // lần thứ 2 trở đi thì nó lấy cái cũ ra dùng thôi
        Singleton anotherSingleton = Singleton.getInstance("BAR");
        System.out.println(anotherSingleton);

        // EagerInitializedSingleton Singleton Class được khởi tạo ngay khi được gọi đến. Đây là cách dễ nhất nhưng nó có một nhược điểm mặc dù instance đã được khởi tạo mà có thể sẽ không dùng tới.
        EagerInitializedSingleton eagerInitializedSingleton = EagerInitializedSingleton.getInstance();
        System.out.println("get eager = "+eagerInitializedSingleton);
        EagerInitializedSingleton eagerInitializedSingleton1 = EagerInitializedSingleton.getInstance();
        System.out.println("get eager = "+eagerInitializedSingleton1);

        StaticBlockSingleton staticBlockSingleton = StaticBlockSingleton.getInstance();
        System.out.println("get static block = " + staticBlockSingleton);
        StaticBlockSingleton staticBlockSingleton1 = StaticBlockSingleton.getInstance();
        System.out.println("get static block = " + staticBlockSingleton1);
    }
}
