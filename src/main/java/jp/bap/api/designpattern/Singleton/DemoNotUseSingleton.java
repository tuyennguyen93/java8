package jp.bap.api.designpattern.Singleton;

public class DemoNotUseSingleton {
    public static void main(String[] args) {
        System.out.println("not use Singleton");
        // lần thứ 1 gọi thì nó gọi hàm khởi tạo
        NormalClass normalClass = new NormalClass("FOO normal");
        System.out.println(normalClass.value);
        // từ lần thứ 2 trở đi thì nó cũng vẫn gọi hàm khởi tạo => gây tốn bộ nhớ
        NormalClass normalClass1 = new NormalClass("BAR normal");
        System.out.println(normalClass1.value);
    }
}
