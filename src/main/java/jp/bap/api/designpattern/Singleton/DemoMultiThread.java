package jp.bap.api.designpattern.Singleton;

public class DemoMultiThread {
    public static void main(String[] args) {
        System.out.println("Đa Luồng");
        Thread threadFoo = new Thread(new ThreadFoo());
        Thread threadBar = new Thread(new ThreadBar());
        threadFoo.start();
        threadBar.start();
    }
// trong 1 class xử dụng đa luồn thì Singleton chạy ko chính xác => sử dụng synchronized
    static class ThreadFoo implements Runnable {
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("FOO");
            System.out.println(singleton.value);
        }
    }

    static class ThreadBar implements Runnable {
        @Override
        public void run() {
            Singleton singleton = Singleton.getInstance("BAR");
            System.out.println(singleton.value);
        }
    }
}
