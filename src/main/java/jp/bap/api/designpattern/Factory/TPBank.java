package jp.bap.api.designpattern.Factory;

public class TPBank implements Bank {
    @Override
    public String getBankName() {
        return "TP Bank";
    }
}
