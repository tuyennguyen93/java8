package jp.bap.api.designpattern.Factory;

public interface Bank {
    String getBankName();
}
