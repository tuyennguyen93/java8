package jp.bap.api.java8;


import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;

import java.text.NumberFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Vector;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class TypeData {


  public static void main(String[] args) throws InterruptedException, ExecutionException {
    System.out.println("Hello, AuthSnippets!");
    System.out.println("Done!");
    Person p1 = new Person(1,"A");
    Person p2 = new Person(2,"B");
    Person p3 = new Person(3,"C");
    Person p4 = new Person(4,"D");
    Person p5 = new Person(5,"E");

    // ArrayList : chủ yếu là để đọc data , vì mỗi lần thêm sửa xóa nó sẽ tạo ra 1 ArrayList mới làm tốn bộ nhớ
    //Create
    List<Person> personList = new ArrayList<>();
    personList.add(p1);
    personList.add(p2);
    personList.add(p3);
    personList.add(p4);
    personList.add(2,p5);  // insert tại vị trí 2
    // Read
    System.out.println("List = " + personList.toString());

    // copy sang list khac
    ArrayList<Person> personList2 =  new ArrayList<>(personList); // cach 1 (dùng cho list)
    personList2.addAll(personList); // cach 2 (dùng cho List)
    personList2.clone(); // cach 3 (dùng cho ArrayList)
    System.out.println("List2 copy = " + personList2.toString());


    // Delete
    // personList.remove(0); // do xóa phần tử đó gây lỗi trong vòng for đang chạy
    // dùng Iterator  để xóa
    Iterator<Person> personIterator = personList.iterator();
    while (personIterator.hasNext()) {
      Person person = personIterator.next();
      if (person.getId().equals(1) && person.getId().equals(2)) personIterator.remove();
    }
    // java 8
    personList.removeIf(person -> person.getId().equals(1));
    //List<Person> pz = personList.stream().filter(person -> !person.getId().equals(1)).collect(Collectors.toList());
    System.out.println(personList.contains(p2));
    //Update
    personList.set(2,p5);
    personList.forEach(System.out::println);
    //sort
    // Collections.sort(personList, Comparator.comparing(Person::getId).reversed());
    // Collections.sort(personList, (o1, o2) -> o1.getName().compareToIgnoreCase(o2.getName()));
    // Collections.sort(personList); // call từ Person implements Comparable

    // anonymous function sort thay thế cho việc Person implements Comparable và Override
    Collections.sort(personList, new Comparator<Person>() {
      @Override
      public int compare(Person o1, Person o2) {
        if (o1.getId() > o2.getId()) return -1;
        if (o1.getId() < o2.getId()) return 1;
        return 0;
      }
    });

    System.out.println("List sort= " + personList.toString());



    // LinkedList: thường xuyên thêm sửa xóa data, cấu trúc của nó là danh sách liên kết đôi
    List<Person> personLinkedList = new LinkedList<>();
    personLinkedList.add(p1);
    personLinkedList.add(p2);
    personLinkedList.add(p3);
    System.out.println("LinkedList = " + personLinkedList);
    // update + delete
    personLinkedList.set(1,p5);
    personLinkedList.add(p3);
    personLinkedList.remove(3);
    // read
    for (Person p:personLinkedList) {
      System.out.println(p);
    }

    // vector : chạy đồng bộ , bảo mật cao hơn, dùng trong đa luồn , tuy nhiên ngay nay họ dùng Collections. synchronizedList() (ngon hơn vector)
    List<Person> personVector = new Vector<>();
    personVector.add(p1);
    personVector.add(p2);
    personVector.add(p3);
    System.out.println("Vector = " + personVector);
    // update + delete
    personVector.set(0,p5);
    personVector.add(p4);
    personVector.remove(3);
    // read
    for (Person p:personVector) {
      System.out.println(p);
    }




    // khác vs list thì HashSet các phần tử là duy nhất, nó tự động sắp xếp tăng dần
    Set<Person> personSet = new HashSet<>();
    personSet.add(p1);
    personSet.add(p3);
    personSet.add(p2);
    personSet.add(p4);

    System.out.println("HashSet = " + personSet.toString());
    // update // delete
    personSet.remove(p1);
    // read
    personSet.forEach(System.out::println);

    // khác với hashSet thì LinkedHashSet đảm bảo thứ tự nhập vào
    Set<Person> personLinkedHashSet = new LinkedHashSet<>();
    personLinkedHashSet.add(p1);
    personLinkedHashSet.add(p3);
    personLinkedHashSet.add(p2);
    personLinkedHashSet.add(p4);
    System.out.println("LinkedHashSet = " + personLinkedHashSet.toString());

    // treeSet nó tự động sắp xếp theo thứ tự tăng dần, ở đây có 2 thuộc tính nên nó sắp tăng dần theo id
    Set<Person> personTreeSet = new TreeSet<>();
    personTreeSet.add(p3);
    personTreeSet.add(p2);
    personTreeSet.add(p1);
    System.out.println("TreeSet = " + personTreeSet.toString());


    // Queue là tập hớp các phần tử giống như xếp hàng,  Phần tử nào đến trước thì đứng trước, phần tử nào thêm sau thì bên dưới.
    Queue<Person> personQueue = new ArrayDeque<>();
    personQueue.add(p1);
    personQueue.add(p2);
    personQueue.add(p3);

    // lấy phần tử đầu queue mà ko làm gì nó
    System.out.println("head of queue = " + personQueue.peek());
    // System.out.println("head and delete of queue = " + personQueue.poll()); // lấy phần tử đầu queue ra và xóa nó
    // delete : thằng nào vào trc thì bị xóa trước => p1 bị xóa
    personQueue.remove();
    // delete bất kì:
    personQueue.remove(p3);
    System.out.println("Queue = " + personQueue.toString());

    // Map sử dụng cặp key/value để thể hiện giá trị. Key là duy nhất và trong HashMap có thể null. Map có thể sử dụng để lưu các giá trị với kiểu dữ liệu tham chiếu
    // put giống key đã có thì nó sẽ chồng lên trên data có key đó
    Map<Integer, String> map = new HashMap<>();
    map.put(1,"A");
    map.put(1,"B");
    map.put(2,"C");
    map.put(3,"C");
    map.put(1,"D");
    // read
    Set<Integer> integerSet = map.keySet(); // lấy key ra
    System.out.println("Map<Interger,String> = " + map.toString());
    for (Integer i:integerSet) {
      System.out.println(map.get(i));
    }
    // lấy ra key và value
    for (Map.Entry<Integer,String> e : map.entrySet()){
      System.out.println(e.getKey());
      System.out.println(e.getValue());
    }

    // remove by value
    map.values().removeIf(value -> !value.contains("1"));
    // remove by key
    map.keySet().removeIf(key -> key != 1);
    // remove by entry / combination of key + value
    map.entrySet().removeIf(entry -> entry.getKey() != 1);

    Map<Integer,Person> mapPerson = new HashMap<>();
    mapPerson.put(1,p1);
    mapPerson.put(2,p2);
    mapPerson.put(3,p3);
    mapPerson.put(1,p4);
    for (Map.Entry<Integer,Person> e : mapPerson.entrySet()) {
      System.out.println(e.getKey());
      System.out.println(e.getValue());
    }
    mapPerson.values().removeIf(value -> value.getName().equals("C"));
    System.out.println(" map after delete = " + mapPerson.toString());

    // giống hashMap tuy nhiên nó sắp xếp tăng dần theo key
    TreeMap<Integer, String> treeMap = new TreeMap<>();
    treeMap.put(1,"A");
    treeMap.put(3,"B");
    treeMap.put(2,"C");
    treeMap.put(6,"D");
    treeMap.put(4,"A");
    treeMap.put(4,"F");
    System.out.println("treeMap = " + treeMap);
    treeMap.remove(4);


  //  StringBuilder : dùng nối chuổi nhanh
  //  StringBuffer : đồng bộ ...

    //Serializable giúp chuyển các đối tượng thành kiểu String để lưu xuống file hoặc truyền qua mạng.
    // Việc này đảm bảo các đối tượng được lưu chính xác và khi chuyển đổi ngược lại chuẩn.
    // https://www.youtube.com/watch?v=T_asbhzim4o&list=PLsfLgp1K1xQ51TV6pCyS9yNQvstVk_le_&index=19

    // Định dạng các số theo phân cách phần ngàn, phần thập phân hoặc theo ý muốn dễ dàng với 2 thư viện hỗ trợ. NumberFormat và DecimalFormat. theo quốc gia
    // định dạng tiền tệ..
    long l = 1234556;
    double d = 12432332.23423;
    Locale locale = new Locale("vi","VI");
    NumberFormat fomat = NumberFormat.getCurrencyInstance(locale);
    System.out.println(" fomat VN"+fomat.format(l));
    System.out.println(" fomat VN"+fomat.format(d));

    // java 8
    // stream từ các nguồn dữ liệu như:  Collections, Arrays hoặc I/O resources, các lớp kế thừa của Collection (List...)
    Collection<String> collection = Arrays.asList("hello", "loda", "kaka");
    Stream<String> streamOfCollection = collection.stream(); // Tạo ra một stream từ collection

    List<Person> people = new ArrayList<>();
    Stream<Person> streamPersion = people.stream(); // tạo ra 1 stream

    // Stream<Person> dùng 1 lần , lần sau dùng sẽ bị lỗi ,Một lưu ý khi sử dụng là Stream không được tái sử dụng. Ví dụ:
    //     Stream<Person> streamPerson = personList.stream();
    //     streamPerson.forEach(person -> System.out.println(person));  // dùng lần 1 ok
    //     streamPerson.forEach(person -> System.out.println(person));  // dùng lần 2 lỗi do streamPerson ko có data


    Stream<Person> streamPerson = personList.stream();

    // forEach: Duyệt qua toàn bộ dữ liệu
       streamPerson.forEach(person -> System.out.println(person));

    // Map: Tạo ra các giá trị mới từ dữ liệu hiện có
     personList.stream().map(Person::getName).forEach(System.out::println);
     List<String> nameLst = personList.stream().map(Person::getName).collect(Collectors.toList());
     List<String> nameLstLower= personList.stream().map(person -> person.getName().toLowerCase()).collect(Collectors.toList());
     System.out.println(" " + nameLstLower.toString());

     // filter() gíup chúng ta thao tác với những dữ liệu mong muốn vd lấy số chẵn
    List<Integer> idLst = personList.stream().filter(person -> person.getId() % 2 == 0).map(Person::getId).collect(Collectors.toList());

    // tao ra 1 list chứa các map
    List<Map<Integer,String>> idMapLst = personList.stream().map(person -> {
      Map<Integer,String> map1 = new HashMap<>();
      map1.put(1,"x");
      map1.put(person.getId(),person.getName());
      return map1;
    }).collect(Collectors.toList());
    // tạo ra 1 map
    Map<Integer,String> map2 = personList.stream().distinct().collect(Collectors.toMap(Person::getId,Person::getName));
    System.out.println(map2.toString());

    // limit Giới hạn số lượng dữ liệu cần xử lý, sorted sắp xếp
    List<Person> personList1 = personList.stream().limit(3).sorted((o1, o2) -> {
      if (o1.getId() > o2.getId()) return 1;
      if (o1.getId() < o2.getId()) return -1;
      return 0;
    }).collect(Collectors.toList());

    // lấy thằng có id lớn nhất => sắp xếp tăng dần lấy thằng đầu tiên hoặc limit 1
    Optional<Person> personOptional = personList.stream().sorted((o1, o2) -> {
      if (o1.getId() > o2.getId()) return 1;
      if (o1.getId() <o2.getId()) return -1;
      return 0;
    }).findFirst();
    // cách khác dùng max
    Optional<Person> personOptional1 = personList.stream().max((o1, o2) -> {
      if (o1.getId() > o2.getId()) return 1;
      if (o1.getId() < o2.getId()) return -1;
      return 0;
    });
    System.out.println(personOptional1.toString());

    // nối thành 1 chuổi cách nhau |
    List<String> list = Arrays.asList("java", "python", "nodejs", "ruby");
    String result = list.stream().map(x -> x).collect(Collectors.joining(" | "));

    // skip bỏ qua phần tử ở vị trí nào đó
    List<String> data = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");
    data.stream() .skip(1) .limit(3).forEach(System.out::print); // C#C++PHP

    //  anyMatch(), allMatch(), noneMatch()
    // anyMatch() trả về một boolean tùy thuộc vào điều kiện được áp dụng trên Stream dữ liệu. Phương thức này trả về true ngay khi phần tử đầu tiên thõa mãn điều kiện, những phần tử còn lại sẽ không được kiểm tra.
    // allMatch() : Phương thức này trả về true nếu tất cả phần tử đều thõa mãn điều kiện. Nếu một phần tử không thõa điều kiện, những phần tử còn lại sẽ không được kiểm tra.
    // noneMatch() : Ngược lại với allMatch(), phương thức này trả về true nếu tất cả phần tử đều không thõa mãn điều kiện. Nếu một phần tử thõa điều kiện, những phần tử còn lại sẽ không được kiểm tra.
    List<String> data1 = Arrays.asList("Java", "C#", "C++", "PHP", "Javascript");
    boolean result1 = data1.stream().anyMatch((s) -> s.equalsIgnoreCase("Java"));
    System.out.println(result1); // true

    // Phương thức count() trả về tổng số phần tử cho dữ liệu luồng.
    List<Integer> data2 = Arrays.asList(2, 3, 5, 4, 6);
    long count = data2.stream().filter(num -> num % 3 == 0).count();
    System.out.println("Count = " + count);

    // Phương thức summaryStatistics() được sử dụng để lấy giá trị count, min, max, sum và average với tập dữ liệu số.
    List<Integer> primes = Arrays.asList(2, 3, 5, 7, 10);
    IntSummaryStatistics stats = primes.stream().mapToInt((x) -> x).summaryStatistics();
    System.out.println("Count: " + stats.getCount());
    System.out.println("Max: " + stats.getMax());
    System.out.println("Min: " + stats.getMin());
    System.out.println("Sum: " + stats.getSum());
    System.out.println("Average: " + stats.getAverage());

    // Phương thức reduce() kết hợp các phần tử luồng thành một bằng cách sử dụng một BinaryOperator(toán tử nhứ phép +,-...).
    int result3 = IntStream.of(1, 2, 3, 4).reduce(10, (a, b) -> a + b); // 10: phần tử ban đầu , a+b xử lý phép cộng(1+2+3+4) , sau đó công cho 10 = 20
    System.out.println(result3); // 20

    // Stream API với I/O:  trên tập tin như đọc file văn bản theo từng dòng (line) cũng có thể áp dụng với Stream API
/*
    String fileName = "lines.txt";

    // read file into stream, try-with-resources
    try (Stream<String> stream = Files.lines(Paths.get(fileName))) {

      stream //
              .onClose(() -> System.out.println("Done!")) //
              .filter(s -> s.startsWith("line3")) //
              .forEach(System.out::println);

    } catch (IOException e) {
      e.printStackTrace();
    }
 */

// test lỗi null point exception
List ac = findAll("18");
    System.out.println("test Collection.emptyList = " + ac);
    if (ac== null) {
      System.out.println("null");
    }
    if (ac.isEmpty()){
      System.out.println("empty");
    }


/*
      Khi muốn viết 1 api mà chứa cả update , create , delete
      Thuật toán xử Lý 2 list
        Integer a[] = {1, 2, 3, 4};
        Integer b[] = {9, 8, 2, 3};
     */

     List<Integer> aList =  Arrays.asList(1, 2, 3, 4);
     List<Integer> bList =  Arrays.asList(9, 8, 2, 3);

     // chuyển qua set để những thằng giống nhau bị loại đi
    Set<Integer> aSet = new HashSet<>(aList);
    Set<Integer> bSet = new HashSet<>(bList);
    System.out.println("IN.aSet = " + aSet.toString());

    /**
     *  delete
     *  => tìm ở mảng a các phần tử không có trong mảng b
     *   Integer c[] = {1, 4}
     */

   // aSet.removeAll(bSet);
    System.out.println("OUT.aSet = " + aSet.toString());


    /**
     *  create
     *  => tìm ở mảng b các phần tử khác mảng a
     *  Integer c[] = {9, 8}
     */
  //  bSet.removeAll(aSet);
    System.out.println("OUT.bSet = " + bSet.toString());

    /**
     *  update
     *  => tìm ở mảng b các phần tử giống mảng a
     *   Integer c[] = {2, 3}
     */
  //  aSet.retainAll(bSet);
    System.out.println("OUT.aSet = " + aSet.toString());

  // gộp 2 mảng
    aSet.addAll(bSet);
  System.out.println("OUT.aSet = " + aSet.toString());


/*
xem các hàm trong Collections, Lists, Maps, Sets
https://vietjack.com/java/thuat_toan_collection_trong_java.jsp
 */
  // đảo ngược
   Collections.reverse(aList);
   System.out.println("OUT.aSet1 = " + aList.toString());
   System.out.println("Min = " + Collections.min(aList)+ " Max = " + Collections.max(aList));













  }
  public static List findAll(String lang) {

    if (lang.equals("1")) {
      List<Person> personList = new ArrayList<>();
      Person p1 = new Person(1,"A");
      Person p2 = new Person(2,"B");
      personList.add(p1);
      personList.add(p2);
      return personList;
    }
    // return null;
      return Collections.EMPTY_LIST; // trả về EMPTY_LIST => khi sử dụng tránh bị lỗi null point exception
    }








  }
