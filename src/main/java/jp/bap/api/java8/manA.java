package jp.bap.api.java8;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class manA {


    public static void main(String[] args) {
        System.out.println("cach 1 = " + getStr.getStr("nomalx", new StringProcessor() {
            @Override
            public String process(String input) {
                return input.toUpperCase();
            }
        }));

        //  input(đầu vào) -> return(đầu ra)
        System.out.println("cach 2 = " + getStr.getStr("nomalx", input -> input.toUpperCase()));

        System.out.println("cach 2' = " + getStr.getStr("nomalx", input -> {
            String s ="1";
            return "1"+s;
        }));

        System.out.println("cach 3 = " + getStr.getStr("nomalx", String::toUpperCase));

        // stream từ các nguồn dữ liệu như:  Collections, Arrays hoặc I/O resources, các lớp kế thừa của Collection (List...)
        Collection<String> collection = Arrays.asList("hello", "loda", "kaka");
        Stream<String> streamOfCollection = collection.stream(); // Tạo ra một stream từ collection

        List<Person> people = new ArrayList<>();
        Stream<Person> streamPersion = people.stream(); // tạo ra 1 stream



    }

}
