package jp.bap.api.java8;

public interface StringProcessor {
    public String process(String input);
}
