package jp.bap.api.java8;

public class getStr {
    public static String getStr(String input, StringProcessor processor){
        return processor.process(input);
    }
}
