package jp.bap.api.java8;

public class Person implements Comparable<Person> {
    private Integer id;
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Person() {
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    // sort theo Id
//    @Override
//    public int compareTo(Person o) {
//        if (this.getId() > o.getId()) return -1;
//        if (this.getId()< o.getId()) return 1;
//        return 0;
//    }

    // implements Comparable<Person> để có thể dùng hàm  Collections.sort(personList);
    // sort theo name
    @Override
    public int compareTo(Person o) {
        return this.getName().compareToIgnoreCase(o.getName());
      //  return -this.getName().compareToIgnoreCase(o.getName()); // reverse
    }
}
