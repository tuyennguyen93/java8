package jp.bap.api.authorization;

import jp.bap.common.EMEnum;
import jp.bap.common.dto.PermissionDTO;
import jp.bap.common.dto.RolePermissionDTO;
import jp.bap.common.dto.UserDTO;
import jp.bap.common.event.UserEvent;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.Role;
import jp.bap.common.model.RolePermission;
import jp.bap.common.model.User;
import jp.bap.common.repository.RolePermissionRepository;
import jp.bap.common.repository.RoleRepository;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.util.SecurityUtil;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuthorizationService {

    @Autowired
    private RolePermissionRepository rolePermissionRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ApplicationContext applicationContext;

    public List<Role> getAllRoles() throws ResourceNotFoundException {
        return roleRepository.findAll();
    }

    public UserDTO setRoleUser(Integer userIdSetRole, EMEnum.RoleName newRole)
            throws BadRequestException, ResourceNotFoundException {

        User userSetRole = userRepository.findById(userIdSetRole)
                .orElseThrow(() -> new ResourceNotFoundException("User", "id", userIdSetRole));

        Role roleFindByName = roleRepository.findByName(newRole)
                .orElseThrow(() -> new ResourceNotFoundException(MessageConstants.ROLE_ALREADY_EXISTS));

        EMEnum.RoleName currentRole = SecurityUtil.getRoleName();
        EMEnum.RoleName userRole = userSetRole.getRole().getName();


        if (EMEnum.RoleName.ROLE_ADMIN.equals(newRole)
                || EMEnum.RoleName.ROLE_ADMIN.equals(userRole)) {

            throw new BadRequestException(MessageConstants.FROZE_ROLE_ADMIN);
        }


        if (EMEnum.RoleName.ROLE_ADMIN.equals(currentRole)) {

            userSetRole.setRole(roleFindByName);
            userSetRole.setRoleId(roleFindByName.getId());

        } else if (!EMEnum.RoleName.ROLE_QCQA.equals(userRole)
                && !EMEnum.RoleName.ROLE_QCQA.equals(newRole)) {

            userSetRole.setRole(roleFindByName);
            userSetRole.setRoleId(roleFindByName.getId());
        } else {
            throw new BadRequestException(MessageConstants.ROLE_PM_NOT_SET_ROLE_QCQA);
        }
        return new UserDTO(userRepository.save(userSetRole));
    }

    public List<RolePermissionDTO> getAllResourcesAndPrivilegesOnTheResourceByRoleId(Integer roleId)
            throws ResourceNotFoundException {

        List<RolePermissionDTO> collect = rolePermissionRepository.getByRoleId(roleId).stream()
                .map(RolePermissionDTO::new)
                .collect(Collectors.toList());
        if (collect.size() > 0) {
            return collect;
        } else {
            throw new ResourceNotFoundException("RolePermission", "roleId", roleId);
        }
    }

    public List<RolePermissionDTO> setRolePermissionForResource
            (List<PermissionDTO> permissionDTOs) {

        applicationContext.publishEvent(new UserEvent(this, SecurityUtil.getUser()));
        Integer roleId = 0;
        for (PermissionDTO permissionDTO : permissionDTOs) {
            RolePermission rolePermission = rolePermissionRepository
                    .findById(permissionDTO.getRolePermissionId())
                    .orElseThrow(
                            () -> new ResourceNotFoundException("RolePermission", "id", permissionDTO.getRolePermissionId()));
            if (!EMEnum.RoleName.ROLE_ADMIN.equals(rolePermission.getRole().getName())) {

                if (EMEnum.ResourceStatus.CAN_EDIT.equals(permissionDTO.getStatus())) {
                    rolePermission.setCanView(true);
                    rolePermission.setCanEdit(true);
                } else if (EMEnum.ResourceStatus.CAN_VIEW.equals(permissionDTO.getStatus())) {
                    rolePermission.setCanView(true);
                    rolePermission.setCanEdit(false);
                } else {
                    rolePermission.setCanView(false);
                    rolePermission.setCanEdit(false);
                }

                rolePermissionRepository.save(rolePermission);
                roleId = rolePermission.getRoleId();
            } else {
                throw new BadRequestException(MessageConstants.FROZE_ROLE_ADMIN);
            }
        }

        return getAllResourcesAndPrivilegesOnTheResourceByRoleId(roleId);
    }
}