package jp.bap.api.authorization;

import io.swagger.annotations.Api;
import jp.bap.common.EMEnum;
import jp.bap.common.dto.PermissionDTO;
import jp.bap.common.model.User;
import jp.bap.common.dto.RolePermissionDTO;
import jp.bap.common.dto.UserDTO;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.Role;
import jp.bap.common.response.APIResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1/admin")
@Api(value = "Admin APIs")
public class AuthorizationController {

    @Autowired
    AuthorizationService authorizationService;

    /**
     * Get a list of all roles
     *
     * @return {@link APIResponse} data is list {@link Role}
     * @throws ResourceNotFoundException
     */
    @GetMapping("/roles/")
    public APIResponse<List<Role>> getAllRoles() throws ResourceNotFoundException {
        return APIResponse.okStatus(authorizationService.getAllRoles());
    }

    /**
     * Show resource and Permission on the resource by roleId
     *
     * @param roleId of {@link Role}
     * @return {@link APIResponse} data is list {@link RolePermissionDTO}
     * @throws ResourceNotFoundException
     */
    @GetMapping("/roles/{roleId}")
    public APIResponse<List<RolePermissionDTO>> getAllResourcesAndPermissionOnTheResourceByRoleId(
            @PathVariable("roleId") Integer roleId)
            throws ResourceNotFoundException {
        return APIResponse.okStatus(authorizationService
                .getAllResourcesAndPrivilegesOnTheResourceByRoleId(roleId));
    }

    /**
     * Update Role and Permission on resource
     *
     * @param permissionDTOs        data is list{@link PermissionDTO}
     * @return {@link APIResponse} data is {@link RolePermissionDTO}
     * @throws ResourceNotFoundException
     * @throws BadRequestException
     */
    @PutMapping ("/rolepermissions/")
    public APIResponse<List<RolePermissionDTO>> updateRolePermission(@Valid
            @RequestBody List<PermissionDTO> permissionDTOs)
            throws ResourceNotFoundException, BadRequestException {
        return APIResponse.okStatus(authorizationService.setRolePermissionForResource(permissionDTOs));
    }

    /**
     * Set role user
     *
     * @param userIdSetRole {@link User}
     * @param newRole       {@link EMEnum.RoleName}
     * @return {@link APIResponse} data is {@link UserDTO}
     * @throws ResourceNotFoundException
     * @throws BadRequestException
     * @apiNote ROLE_PM can't set role for user is ROLE_QCQA
     */
    @PutMapping("/roles/")
    public APIResponse<UserDTO> setRoleUser(
            @RequestParam(value = "userIdSetRole", required = false) Integer userIdSetRole,
            @RequestParam(value = "newRole", required = false) EMEnum.RoleName newRole)
            throws BadRequestException, ResourceNotFoundException {
        return APIResponse.okStatus(authorizationService.setRoleUser(userIdSetRole, newRole));
    }
}
