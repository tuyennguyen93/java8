package jp.bap.api.thread;

import lombok.SneakyThrows;

public class DeadLock implements Runnable {
    public synchronized void func1() throws InterruptedException {
        System.out.println("từ 1 call 2");
        func2();
    }

    public synchronized void func2() throws InterruptedException {
        System.out.println("từ 2 call 1");
        func1();
    }
    @SneakyThrows
    @Override
    public void run() {
        func1();
    }

    public static void main(String[] args) {
        DeadLock d = new DeadLock();
        Thread t1 = new Thread(d);
        Thread t2 = new Thread(d);
        t1.start();
        t2.start();

    }
}
