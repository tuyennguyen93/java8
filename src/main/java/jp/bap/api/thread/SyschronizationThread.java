package jp.bap.api.thread;

import lombok.SneakyThrows;

public class SyschronizationThread implements Runnable {
    public int tong;
    public SyschronizationThread() {
        this.tong = 1000;
    }

   // @Synchronized
    public synchronized  void rutTien() throws InterruptedException { // khai báo synchronized để synchronized :
        if (tong>0) {
            Thread.sleep(1000); // cho đóng băng thread 1s mới thấy sự khác biệt
            tong = tong -1000;
            System.out.println(tong);
        } else {
            System.out.println("het tien roi!");
        }
    }

    @SneakyThrows
    @Override
    public void run(){
        rutTien();
    }

    public static void main(String[] args) {
        SyschronizationThread t1 = new SyschronizationThread();
        Thread thread1 = new Thread(t1);
        Thread thread2 = new Thread(t1);
        thread1.start();
        thread2.start();
        // rõ ràng thread1 và thread2 không đồng bộ với nhau dẫn đến truy xuất tới hàm rutTien() là khác nhau nên có sự sai khác
        // synchronized : nghĩa là khi thread nào gọi hàm này thì thread khác phải chờ nó hoàn thành đã mới đc gọi
    }
}
