package jp.bap.api.thread;

import lombok.SneakyThrows;
import lombok.Synchronized;

import javax.transaction.Synchronization;

public class Asynchronization implements Runnable {
    public int tong;
    public Asynchronization() {
        this.tong = 1000;
    }

   // @Synchronized
    public void rutTien() throws InterruptedException {
        if (tong>0) {
            Thread.sleep(1000); // cho đóng băng thread 1s mới thấy sự khác biệt
            tong = tong -1000;
            System.out.println(tong);
        } else {
            System.out.println("het tien roi!");
        }
    }

    @SneakyThrows
    @Override
    public void run(){
        rutTien();
    }

    public static void main(String[] args) {
        Asynchronization t1 = new Asynchronization();
        Thread thread1 = new Thread(t1);
        Thread thread2 = new Thread(t1);
        thread1.start();
        thread2.start();
        // rõ ràng thread1 và thread2 không đồng bộ với nhau dẫn đến truy xuất tới hàm rutTien() là khác nhau nên có sự sai khác
    }
}
