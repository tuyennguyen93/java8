package jp.bap.api.thread;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        // cách tạo thread 1 : implements Runnable
        Mytheard1 th1 = new Mytheard1();
        Thread t = new Thread(th1);

        // cách tạo thread 2 : extends Thread
        Mytheard2 th2 = new Mytheard2();

        // chạy thread
       // t.start();
       // th2.start();

        // thread join sẽ cho phép thread đó thực thi xong chương trình của nó, sau đó mới cho phép các thread còn lại thực thi.
        Mytheard2 t1 = new Mytheard2();
        Mytheard3 t2 = new Mytheard3();
        Mytheard4 t3 = new Mytheard4();
//        t1.start();
//        t1.join(); // chờ thằng 1 chạy xong hoặc chạy bao nhiêu ms thì thằng t2 , t3 ms được chạy t1.join(1500)
//        t2.start();
//        t3.start();

        System.out.println("---setPriority---");
        // Thread trong java được sắp sếp theo ưu tiến từ 1 đến 10. Tuỳ thuộc vào mức độ ưu tiên,
        // thread được chạy trước hay sau. Tuy vậy không đảm báo thứ tự ưu tiên này trong thực tế.
        t1.setPriority(1);
        t2.setPriority(10);
        t3.setPriority(5);
        t1.start();
        t2.start();
        t3.start();

        // Daemon thread là các thread chạy ngầm phía dưới (background) như chương trình dọn rác..
        System.out.println("---setDaemon---");
        t1.setDaemon(true);
        System.out.println(t1.isDaemon());



    }
}
