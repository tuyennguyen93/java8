package jp.bap.api.user;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import jp.bap.common.EMEnum;
import jp.bap.common.aspect.PermissionCheck;
import jp.bap.common.dto.*;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.PaginationSortingException;
import jp.bap.common.response.APIResponse;
import jp.bap.common.response.APIResponseError;

import jp.bap.common.util.SecurityUtil;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import jp.bap.common.model.User;
import jp.bap.common.repository.EmployeeRepository;
import jp.bap.common.repository.RoleRepository;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.security.JwtTokenProvider;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import jp.bap.common.dto.UserDTO;
import jp.bap.common.exception.ResourceNotFoundException;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/api/v1/users")
@Api(value = "User APIs")
public class UserController {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtTokenProvider tokenProvider;

    @Autowired
    private UserService userService;

    @Value("${app.excel.name}")
    protected String fileName;

    @ApiOperation(value = "Đăng Nhập", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.SUCCESS_CODE, message = MessageConstants.SUCCESS),
            @ApiResponse(code = APIConstants.UNAUTHORIZED_CODE, message = MessageConstants.NOT_AUTHENTICATED),
            @ApiResponse(code = APIConstants.FORBIDDEN_CODE, message = MessageConstants.ACCESS_DENIED),
            @ApiResponse(code = APIConstants.NOT_FOUND_CODE, message = MessageConstants.NOT_FOUND),
            @ApiResponse(code = APIConstants.BAD_REQUEST_CODE, message = MessageConstants.BAD_REQUEST),
            @ApiResponse(code = APIConstants.INTERNAL_SERVER_ERROR_CODE, message = MessageConstants.SERVER_ERROR)
    })
    @PostMapping("/signin")
    public APIResponse<?> authenticateUser(@Valid @RequestBody LoginRequestDto loginRequest) {
        Authentication authentication;
        try {
            authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            loginRequest.getEmail(),
                            loginRequest.getPassword()
                    )
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (DisabledException | LockedException | BadCredentialsException e) {
            if (e.getClass().equals(BadCredentialsException.class)) {
                APIResponseError apiResponseError = APIResponseError.builder()
                        .code(HttpStatus.BAD_REQUEST.value())
                        .message(MessageConstants.INVALID_EMAIL_PASSW)
                        .build();
                return APIResponse.errorStatus(apiResponseError, HttpStatus.BAD_REQUEST);

            } else if (e.getClass().equals(DisabledException.class)) {
                return APIResponse.errorStatus(
                        APIConstants.BAD_REQUEST_CODE,
                        MessageConstants.DISABLE,
                        HttpStatus.BAD_REQUEST
                );
            } else if (e.getClass().equals(LockedException.class)) {
                APIResponseError apiResponseError = APIResponseError.builder()
                        .code(HttpStatus.BAD_REQUEST.value())
                        .message(MessageConstants.LOCKED)
                        .build();
                return APIResponse.errorStatus(apiResponseError, HttpStatus.BAD_REQUEST);
            } else {
                return APIResponse.errorStatus(
                        APIConstants.BAD_REQUEST_CODE,
                        MessageConstants.UNKNOWN,
                        HttpStatus.BAD_REQUEST
                );
            }
        }
        List<String> permissions = userService.getAllResourcesAndPrivilegesOnTheResourceByRoleId(SecurityUtil.getRoleName());
        String jwt = tokenProvider.generateToken(authentication);
        return APIResponse.okStatus(new JwtAuthenticationStatusDto(jwt,permissions));
    }

    /* Đăng ký cho member */
    @ApiOperation(value = "Đăng Ký", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.SUCCESS_CODE, message = MessageConstants.SUCCESS),
            @ApiResponse(code = APIConstants.UNAUTHORIZED_CODE, message = MessageConstants.NOT_AUTHENTICATED),
            @ApiResponse(code = APIConstants.FORBIDDEN_CODE, message = MessageConstants.ACCESS_DENIED),
            @ApiResponse(code = APIConstants.NOT_FOUND_CODE, message = MessageConstants.NOT_FOUND),
            @ApiResponse(code = APIConstants.BAD_REQUEST_CODE, message = MessageConstants.BAD_REQUEST),
            @ApiResponse(code = APIConstants.INTERNAL_SERVER_ERROR_CODE, message = MessageConstants.SERVER_ERROR)
    })
    @PostMapping("/signup")
    public APIResponse<ApiStatusDto> registerUser(@Valid @RequestBody SignUpRequestDto signUpRequest) throws BadRequestException {
        return APIResponse.okStatus(userService.saveUser(signUpRequest));
    }

    @GetMapping("/me")
    @ApiOperation(value = "Profile", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.SUCCESS_CODE, message = MessageConstants.SUCCESS),
            @ApiResponse(code = APIConstants.UNAUTHORIZED_CODE, message = MessageConstants.NOT_AUTHENTICATED),
            @ApiResponse(code = APIConstants.FORBIDDEN_CODE, message = MessageConstants.ACCESS_DENIED),
            @ApiResponse(code = APIConstants.NOT_FOUND_CODE, message = MessageConstants.NOT_FOUND),
            @ApiResponse(code = APIConstants.BAD_REQUEST_CODE, message = MessageConstants.BAD_REQUEST),
            @ApiResponse(code = APIConstants.INTERNAL_SERVER_ERROR_CODE, message = MessageConstants.SERVER_ERROR)
    })
    @PermissionCheck(resource = EMEnum.ResourceName.USER, canEdit = true, canView = true)
    public APIResponse<?> getInfoUser() {
        if (SecurityUtil.getUser() != null) {
            return APIResponse.okStatus(userService.convertUserData(SecurityUtil.getUser()));
        }
        return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);
    }


    /**
     * Get a list of all users
     *
     * @return {@link APIResponse} data is list of {@link UserDTO}
     * @throws ResourceNotFoundException
     */
    @GetMapping("/")
    public APIResponse<PageInfo<UserDTO>> getAllUsers(
            @RequestParam(defaultValue = APIConstants.PAGE_NUMBER_DEFAULT) Integer pageNo,
            @RequestParam(defaultValue = APIConstants.PAGE_SIZE_DEFAULT) Integer pageSize) throws PaginationSortingException, BadRequestException {

        return APIResponse.okStatus(this.userService.getAllUsers(pageNo, pageSize));

    }

    /**
     * Get user by id
     *
     * @param id {@link User } id
     * @return {@link APIResponse} data is {@link UserDTO}
     * @throws ResourceNotFoundException
     */
    @GetMapping("/{id}")
    public APIResponse<UserDTO> getUserById(@PathVariable("id") Integer id) throws ResourceNotFoundException {

        return APIResponse.okStatus(this.userService.getUserById(id));

    }

    /**
     * In/active one or more users by id
     *
     * @param ids is array {@link User } id
     * @return {@link APIResponse} data is {@link UserDTO}
     * @throws ResourceNotFoundException
     */
    @PutMapping("/active")
    public APIResponse<List<UserDTO>> isActiveUser(@RequestParam Integer... ids) throws ResourceNotFoundException {

        return APIResponse.okStatus(userService.isActiveUser(ids));

    }

    @PutMapping("/password-reset")
    @ApiOperation(value = "Change Password ", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.SUCCESS_CODE, message = MessageConstants.SUCCESS),
            @ApiResponse(code = APIConstants.UNAUTHORIZED_CODE, message = MessageConstants.NOT_AUTHENTICATED),
            @ApiResponse(code = APIConstants.FORBIDDEN_CODE, message = MessageConstants.ACCESS_DENIED),
            @ApiResponse(code = APIConstants.NOT_FOUND_CODE, message = MessageConstants.NOT_FOUND),
            @ApiResponse(code = APIConstants.BAD_REQUEST_CODE, message = MessageConstants.BAD_REQUEST),
            @ApiResponse(code = APIConstants.INTERNAL_SERVER_ERROR_CODE, message = MessageConstants.SERVER_ERROR)
    })
    public APIResponse<?> changePassword(@RequestBody ChangePasswordParam param) {
        if (SecurityUtil.getUser() != null) {
            return APIResponse.okStatus(userService.changePasswordUser(param, SecurityUtil.getUser()));
        }
        return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);

    }

    @ApiOperation(value = "Import User", response = List.class)
    @ApiResponses(value = {
            @ApiResponse(code = APIConstants.SUCCESS_CODE, message = MessageConstants.SUCCESS),
            @ApiResponse(code = APIConstants.UNAUTHORIZED_CODE, message = MessageConstants.NOT_AUTHENTICATED),
            @ApiResponse(code = APIConstants.FORBIDDEN_CODE, message = MessageConstants.ACCESS_DENIED),
            @ApiResponse(code = APIConstants.NOT_FOUND_CODE, message = MessageConstants.NOT_FOUND),
            @ApiResponse(code = APIConstants.BAD_REQUEST_CODE, message = MessageConstants.BAD_REQUEST),
            @ApiResponse(code = APIConstants.INTERNAL_SERVER_ERROR_CODE, message = MessageConstants.SERVER_ERROR)
    })
    @PostMapping("/import-user")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public APIResponse<?> importUser(@RequestParam("file") MultipartFile file) throws IOException {
        return APIResponse.okStatus(userService.importUser(file));
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping("/password-reset/admin")
    public APIResponse<?> resetPasswordUserByAdmin(@RequestParam("userId") Integer userId,
                                                     @RequestParam("newPassword") String newPassword) {
        if (SecurityUtil.getUser() != null) {
            return APIResponse.okStatus(userService.resetPasswordUserByAdmin(userId, newPassword));
        }
        return APIResponse.errorStatus(APIConstants.UNAUTHORIZED_CODE, MessageConstants.NOT_AUTHENTICATED, HttpStatus.UNAUTHORIZED);

    }

    @GetMapping(value = "/download/template")
    public ResponseEntity<InputStreamResource> excelTemplate() throws IOException {

        return ResponseEntity.ok()
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .header(HttpHeaders.CONTENT_DISPOSITION)
                .body( userService.dowloadTemp(fileName));
    }

    @GetMapping(value="/download/template1")
    public ResponseEntity<ByteArrayResource> downloadTemplate() throws Exception {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Workbook wb = new XSSFWorkbook();

            Sheet sheet = wb.createSheet("sheet1");
            int i = 0;
            // Create header row
            Row row = sheet.createRow(i);
            // no. column

            row.createCell(0).setCellValue("FullName");

            row.createCell(1).setCellValue("Email");

            row.createCell(2).setCellValue("Password");

            row.createCell(3).setCellValue("PhoneNumber");

            row.createCell(4).setCellValue("dob");

            wb.write(stream);
            stream.close();

            HttpHeaders header = new HttpHeaders();
            header.setContentType(new MediaType("application", "force-download"));
            header.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=Template.xlsx");

            return new ResponseEntity<>(new ByteArrayResource(stream.toByteArray()),
                    header, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
