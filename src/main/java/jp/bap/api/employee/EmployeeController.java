package jp.bap.api.employee;

import jp.bap.api.employee.dto.Employee2Req;
import jp.bap.api.employee.dto.Employee2Salary2Res;
import jp.bap.api.employee.dto.EmployeeReq;
import jp.bap.api.employee.entity.Employee2Salary2PKey;
import jp.bap.common.response.APIResponse;
import jp.bap.common.util.constant.APIConstants;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping("/employees")
    public APIResponse<?> getEmployee()  {
        return APIResponse.okStatus( employeeService.getAllEmployees());
    }

    @GetMapping("/employees/{id}")
    public APIResponse<?> getEmployeeById(@PathVariable("id") Integer id) throws Exception {
        return APIResponse.okStatus(employeeService.getEmployeesById(id));
    }

    @PostMapping("employees")
    public APIResponse<?> createEmployee(@RequestBody EmployeeReq employeeReq){
        return APIResponse.okStatus(employeeService.createEmployee(employeeReq));
    }

    @PutMapping("/employees/{id}")
    public APIResponse<?> updateEmployee(@PathVariable("id") Integer id,
                                         @RequestBody EmployeeReq employeeReq){
        return APIResponse.okStatus(employeeService.updateEmployee(employeeReq,id));
    }

    @DeleteMapping("/employees/{id}")
    public APIResponse<?> deleteEmployee(@PathVariable("id") Integer id){
        employeeService.deleteEmployee(id);
        return APIResponse.okStatus(null);
    }

    @GetMapping("employees/search")
    public APIResponse<?> searchEmployee(@RequestParam(value = "name",required = false) String name , Pageable pageable){
        return APIResponse.okStatus(employeeService.searchEmployee(name,pageable));
    }

    /**
     * test Employee2 salary 2  (table between)
     */
    @GetMapping("/employees2salary2")
    public APIResponse<?> getAllTbBetween()  {
        return APIResponse.okStatus( employeeService.getAllTbBetween());
    }

    @PostMapping("/employees2salary2")
    public APIResponse<?> createTbBetween(@RequestBody Employee2Salary2PKey employee2Salary2PKey){
        return APIResponse.okStatus(employeeService.createTbBetween(employee2Salary2PKey));
    }

//    @PutMapping("/employees2salary2/{employeeId}/{salaryId}")
//    public APIResponse<?> updateTbBetween(@PathVariable("employeeId") Integer employeeId,
//                                          @PathVariable("salaryId") Integer salaryId,
//                                          @RequestBody Employee2Salary2Res employee2Salary2Res) {
//        employeeService.updateTbBetween(employeeId, salaryId, employee2Salary2Res);
//        return APIResponse.okStatus(null);
//    }

    @DeleteMapping("/employees2salary2/{employeeId}/{salaryId}")
    public APIResponse<?> deleteTbBetween(@PathVariable("employeeId") Integer employeeId,
                                          @PathVariable("salaryId") Integer salaryId) {
        employeeService.deleteTbBetween(employeeId,salaryId);
        return APIResponse.okStatus(null);
    }


    // get tb Employee 2 + list Salary2
    @GetMapping("/employees2")
    public APIResponse<?> getEmployee2() {
        return APIResponse.okStatus(employeeService.getEmployee2());
    }

    @PostMapping("employees2")
    public APIResponse<?> createEmployee2(@RequestBody EmployeeReq employeeReq){
        return APIResponse.okStatus(employeeService.createEmployee2(employeeReq));
    }

    @PutMapping("/employees2/{id}")
    public APIResponse<?> updateEmployee2(@PathVariable("id") Integer id,
                                         @RequestBody Employee2Req employee2eq){
        return APIResponse.okStatus(employeeService.updateEmployee(employee2eq,id));
    }

    @DeleteMapping("/employees2/{id}")
    public APIResponse<?> deleteEmployee2(@PathVariable("id") Integer id) {
        employeeService.deleteEmployee2(id);
        return APIResponse.okStatus(true);
    }





}
