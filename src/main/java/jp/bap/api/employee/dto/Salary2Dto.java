package jp.bap.api.employee.dto;

import jp.bap.api.employee.entity.Employee2;
import jp.bap.common.base.BaseModel;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Data
@NoArgsConstructor
public class Salary2Dto  {
    private Integer id;

    private Integer salary;


}
