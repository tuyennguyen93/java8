package jp.bap.api.employee.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Data
public class Employee2Req {
    String name;
    List<Salary2Dto> salary2Dtos = new ArrayList<>();
}
