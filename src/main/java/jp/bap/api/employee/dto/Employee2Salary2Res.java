package jp.bap.api.employee.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Employee2Salary2Res {
    Integer employeeId;
    Integer salaryId;
}
