package jp.bap.api.employee.dto;

import jp.bap.common.model.Salary;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class Employee2Res {
    Integer id;
    String name;
    List<Salary2Dto> salary2Dtos = new ArrayList<>();
}
