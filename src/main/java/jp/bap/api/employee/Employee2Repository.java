package jp.bap.api.employee;

import jp.bap.api.employee.entity.Employee2;
import jp.bap.common.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface Employee2Repository extends JpaRepository<Employee2, Integer> {

//    @Query("select e from Employee e where :name is null or :name like '' or e.fullName  like %:name% ")
//    Page<Employee> searchEmployee(@Param("name") String name, Pageable pageable);
}
