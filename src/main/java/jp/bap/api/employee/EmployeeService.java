package jp.bap.api.employee;

import jp.bap.api.employee.dto.Employee2Req;
import jp.bap.api.employee.dto.Employee2Res;
import jp.bap.api.employee.dto.Employee2Salary2Res;
import jp.bap.api.employee.dto.EmployeeReq;
import jp.bap.api.employee.dto.EmployeeRes;
import jp.bap.api.employee.dto.Salary2Dto;
import jp.bap.api.employee.entity.Employee2;
import jp.bap.api.employee.entity.Employee2Salary2;
import jp.bap.api.employee.entity.Employee2Salary2PKey;
import jp.bap.api.employee.entity.Salary2;
import jp.bap.api.employee.mapper.EmployeeMapper;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.exception.ResourceNotFoundException;
import jp.bap.common.model.Employee;
import jp.bap.common.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    private final EmployeeMapper employeeMapper;

    private final Employee2Salary2Repository employee2Salary2Repository;

    private final Employee2Repository employee2Repository;


    List<EmployeeRes> getAllEmployees() {
        return employeeMapper.convertDto(employeeRepository.findAll());
    }

    EmployeeRes getEmployeesById(Integer id) throws Exception {
        return employeeMapper.convertDto(employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Id")));
    }

    EmployeeRes createEmployee(EmployeeReq employeeReq) {
        Employee employee = employeeRepository.save(employeeMapper.covertEntity(employeeReq));
        return employeeMapper.convertDto(employee);
    }

    EmployeeRes updateEmployee(EmployeeReq employeeReq, Integer id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Id"));
        employee.setFullName(employeeReq.getName());
        Employee employeeSave = employeeRepository.save(employee);
        return employeeMapper.convertDto(employeeSave);
    }

    void deleteEmployee(Integer id) {
        try {
            employeeRepository.deleteById(id);
        } catch (Exception ex) {
            throw new ResourceNotFoundException("Id");
        }
    }

    PageInfo<EmployeeRes> searchEmployee(String name, Pageable pageable) {
        return employeeMapper.convertDto(employeeRepository.searchEmployee(name, pageable));
    }

    List<Employee2Salary2Res> getAllTbBetween() {
        return employeeMapper.convertDto1(employee2Salary2Repository.findAll());
    }

    Employee2Salary2 createTbBetween(Employee2Salary2PKey employee2Salary2PKey) {
        Employee2Salary2 employee2Salary2 = new Employee2Salary2();
        employee2Salary2.setEmployee2Id(employee2Salary2PKey.getEmployee2Id());
        employee2Salary2.setSalary2Id(employee2Salary2PKey.getSalary2Id());
        try {
            return employee2Salary2Repository.save(employee2Salary2);
        } catch (Exception ex) {
            throw new ResourceNotFoundException("salaryId or employeeId not found");
        }
    }


    void deleteTbBetween(Integer employeeId, Integer salaryId) {

        try {

            // cach 1:
            employee2Salary2Repository.deleteByEmployee2IdAndSalary2Id(employeeId, salaryId);
            // id bay gio là có the hieu employee2Salary2PKey
//            Employee2Salary2PKey employee2Salary2PKey = new Employee2Salary2PKey();
//            employee2Salary2PKey.setEmployee2Id(employeeId);
//            employee2Salary2PKey.setSalary2Id(salaryId);
//            employee2Salary2Repository.deleteById(employee2Salary2PKey);
        } catch (Exception ex) {
            throw new ResourceNotFoundException("key");
        }
    }

    void updateTbBetween(Integer employeeId, Integer salaryId, Employee2Salary2Res employee2Salary2Res) {

        Employee2Salary2PKey employee2Salary2PKey = new Employee2Salary2PKey();
        employee2Salary2PKey.setEmployee2Id(employeeId);
        employee2Salary2PKey.setSalary2Id(salaryId);

        Optional<Employee2Salary2> employee2Salary2 = Optional.ofNullable(employee2Salary2Repository.findById(employee2Salary2PKey).orElseThrow(() -> new ResourceNotFoundException("key")));
        employee2Salary2.get().setEmployee2Id(3);
        employee2Salary2.get().setSalary2Id(4);
        employee2Salary2Repository.save(employee2Salary2.get());
    }


    List<Employee2Res> getEmployee2() {
        List<Employee2> employee2 = employee2Repository.findAll();
        return employeeMapper.convertDto2(employee2);
    }

    Employee2 createEmployee2(EmployeeReq employeeReq) {
        Employee2 employee2 = new Employee2();
        employee2.setName(employeeReq.getName());
        return employee2Repository.save(employee2);
    }

    Employee2Res updateEmployee(Employee2Req employee2Req, Integer id) {
        Employee2 employee2 = employee2Repository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Id"));
        employee2.setName(employee2Req.getName());

        // setSalary2List ?? => khi save ở employee2 nó còn có thể save ở các bảng liên kết vs nó (Salary2, emloyee2_salary2)
        List<Salary2Dto> salary2DtoList = (employee2Req.getSalary2Dtos());
        List<Salary2> salary2List = employee2.getSalary2List();
        Salary2 salary2 = new Salary2();
        salary2.setSalary(1);
        salary2.setId(12);

        salary2List.add(salary2); // tạo ra Salary2 mới và save
        salary2List.remove(1); // xóa ở emloyee2_salary2


        employee2.setSalary2List(salary2List);

        Employee2 employeeSave = employee2Repository.save(employee2);
        return employeeMapper.convertDto2(employeeSave);
    }

    void deleteEmployee2(Integer id) {
        try {
            employee2Repository.deleteById(id);
        } catch (DataIntegrityViolationException ex) {
            throw new BadRequestException("Cannot delete ");
        } catch (Exception ex) {
            throw new ResourceNotFoundException("Id");
        }
    }



}
