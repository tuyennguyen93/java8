package jp.bap.api.employee.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee2Salary2PKey implements Serializable {
    private static final long serialVersionUID = 1L;
    private Integer employee2Id;
    private Integer salary2Id;
}
