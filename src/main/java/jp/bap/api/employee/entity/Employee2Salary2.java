package jp.bap.api.employee.entity;

import jp.bap.common.base.BaseModel;
import jp.bap.common.model.Employee;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "employee2_salary2")
@IdClass(Employee2Salary2PKey.class)
public class Employee2Salary2  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "employee2_id")
    private Integer employee2Id;

    @Id
    @Column(name = "salary2_id")
    private Integer salary2Id;

    @Column(name = "created")
    @CreationTimestamp
    private LocalDateTime created;

    @Column(name = "modified")
    @UpdateTimestamp
    private LocalDateTime modified;

//    @NotFound(action = NotFoundAction.IGNORE)
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "employee_id")
//    private Employee employee;

}
