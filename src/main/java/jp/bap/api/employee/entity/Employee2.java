package jp.bap.api.employee.entity;

import jp.bap.common.base.BaseModel;
import jp.bap.common.model.Employee;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@Table(name = "employee2")
public class Employee2 extends BaseModel {

    @Column(name = "name")
    private String name;

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "employee_id")
//    private Employee employee;


    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(
            name = "employee2_salary2",
            joinColumns = @JoinColumn(name = "employee2_id"),
            inverseJoinColumns = @JoinColumn(name = "salary2_id"))
    List<Salary2> salary2List;
}
