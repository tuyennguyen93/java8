package jp.bap.api.employee.entity;

import jp.bap.common.base.BaseModel;
import jp.bap.common.model.Employee;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.List;

@Entity
@Data
@Table(name = "salary2")
public class Salary2 extends BaseModel {

    @Column(name = "salary")
    private Integer salary;

//    @NotFound(action = NotFoundAction.IGNORE)
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "employee_id")
//    private Employee employee;

    @ManyToMany(mappedBy = "salary2List")
    List<Employee2> employee2List;

}
