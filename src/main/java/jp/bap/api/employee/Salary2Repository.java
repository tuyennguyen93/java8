package jp.bap.api.employee;

import jp.bap.api.employee.entity.Salary2;
import jp.bap.common.model.Salary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Salary2Repository extends JpaRepository<Salary2,Integer> {

}
