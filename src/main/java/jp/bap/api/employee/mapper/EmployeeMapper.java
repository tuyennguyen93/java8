package jp.bap.api.employee.mapper;

import jp.bap.api.employee.dto.Employee2Res;
import jp.bap.api.employee.dto.Employee2Salary2Res;
import jp.bap.api.employee.dto.EmployeeReq;
import jp.bap.api.employee.dto.EmployeeRes;
import jp.bap.api.employee.dto.Salary2Dto;
import jp.bap.api.employee.entity.Employee2;
import jp.bap.api.employee.entity.Employee2Salary2;
import jp.bap.api.employee.entity.Salary2;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeMapper {
    public EmployeeRes convertDto(Employee employee){
        EmployeeRes employeeRes = new EmployeeRes();
        employeeRes.setId(employee.getId());
        employeeRes.setName(employee.getFullName());
        return employeeRes;
    }

    public List<EmployeeRes> convertDto(List<Employee> employee){
        List<EmployeeRes> employeeResList = new ArrayList<>();
        employee.forEach(i -> employeeResList.add(convertDto(i)));
        return employeeResList;
    }

    public Employee covertEntity(EmployeeReq employeeReq){
        Employee employee = new Employee();
        employee.setFullName(employeeReq.getName());
        return employee;
    }

    public PageInfo<EmployeeRes> convertDto(Page<Employee> page){
        PageInfo<EmployeeRes> pageInfo = new PageInfo<>() ;
        pageInfo.setTotal(page.getTotalElements());
        pageInfo.setPageSize(page.getSize());
        pageInfo.setTotalPage(page.getTotalPages());
        pageInfo.setPage(page.getNumber());
        pageInfo.setContent(convertDto(page.getContent()));
        return pageInfo;
    }

    public Employee2Salary2Res covertDto1(Employee2Salary2 employee2Salary2) {
        Employee2Salary2Res employee2Salary2Res = new Employee2Salary2Res();
        employee2Salary2Res.setEmployeeId(employee2Salary2.getEmployee2Id());
        employee2Salary2Res.setSalaryId(employee2Salary2.getSalary2Id());
        return employee2Salary2Res;
    }

    public List<Employee2Salary2Res> convertDto1(List<Employee2Salary2> all) {
        List<Employee2Salary2Res> lst = new ArrayList<>();
        all.forEach(i -> lst.add(covertDto1(i)));
        return lst;
    }

    public Salary2Dto covertDtoSalary(Salary2 salary2) {
        Salary2Dto salary2Dto = new Salary2Dto();
        salary2Dto.setId(salary2.getId());
        salary2Dto.setSalary(salary2.getSalary());
        return salary2Dto;
    }

    public List<Salary2Dto> convertDtoListSalary(List<Salary2> salary2) {
        List<Salary2Dto> salary2Dtos = new ArrayList<>();
        salary2.forEach(i -> salary2Dtos.add(this.covertDtoSalary(i)));
        return salary2Dtos;
    }
    public Employee2Res convertDto2(Employee2 employee){
        Employee2Res employeeRes = new Employee2Res();
        employeeRes.setId(employee.getId());
        employeeRes.setName(employee.getName());

        employeeRes.setSalary2Dtos(this.convertDtoListSalary(employee.getSalary2List()));
        return employeeRes;
    }

    public List<Employee2Res> convertDto2(List<Employee2> employee){
        List<Employee2Res> employeeResList = new ArrayList<>();
        employee.forEach(i -> employeeResList.add(convertDto2(i)));
        return employeeResList;
    }

}
