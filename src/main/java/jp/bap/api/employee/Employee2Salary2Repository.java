package jp.bap.api.employee;

import jp.bap.api.employee.dto.Employee2Salary2Res;
import jp.bap.api.employee.entity.Employee2Salary2;
import jp.bap.api.employee.entity.Employee2Salary2PKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public interface Employee2Salary2Repository extends JpaRepository<Employee2Salary2, Employee2Salary2PKey> {

//    @Query("select e from Employee e where :name is null or :name like '' or e.fullName  like %:name% ")
//    Page<Employee> searchEmployee(@Param("name") String name, Pageable pageable);


//    @Transactional
//    @Query(value = " DELETE from employee2_salary2 where employee2_id= :employee2Id and salary2_id= :salary2Id ", nativeQuery = true)
//    void deleteByEmployee2IdAndSalary2Id(@Param("employee2Id") Integer employee2Id, @Param("salary2Id") Integer salary2Id);

    @Transactional
    void deleteByEmployee2IdAndSalary2Id(Integer employeeId, Integer salaryId);

//    @Query(value = " select * from employee2_salary2 where employee2_id= :employeeId2Res and salary2_id= :salaryId2Res LIMIT 1 ", nativeQuery = true)
//    Optional<Employee2Salary2> findByEmployee2IdAndSalary2Id(@Param("employee2Id") Integer employee2Id, @Param("salary2Id") Integer salary2Id);

    @Query(value = " UPDATE  employee2_salary2 " +
            " SET employee2_salary2.employee2_id = :employeeId2Res ," +
            "  employee2_salary2.salary2_id = :salaryId2Res " +
            " WHERE employee2_salary2.employee2_id = :employee2Id and employee2_salary2.salary2_id = :salary2Id ", nativeQuery = true)
    void updateByEmployee2andSalary2(@Param("employee2Id") Integer employee2Id,
                                     @Param("salary2Id") Integer salary2Id,
                                     @Param("employeeId2Res") Integer employeeId2Res,
                                     @Param("salaryId2Res") Integer salaryId2Res);
}
