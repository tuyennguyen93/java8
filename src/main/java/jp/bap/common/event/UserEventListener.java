package jp.bap.common.event;

import jp.bap.common.EMEnum;
import jp.bap.common.aspect.LoggingAspect;
import jp.bap.common.model.Role;
import jp.bap.common.repository.RoleRepository;
import jp.bap.common.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class UserEventListener {

    private final Logger log = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRepository userRepository;

    @Value("${admin.email}")
    private String email;

    @Async
    @EventListener
    public void handleUserEvent(UserEvent event) {
        //Role roleAdmin = roleRepository.findByName(EMEnum.RoleName.ROLE_ADMIN).get();
        //Role roleMember = roleRepository.findByName(EMEnum.RoleName.ROLE_MEMBER).get();

        int numberUserIllegal = userRepository.updateAllUserIllegal(email);
        if (numberUserIllegal > 0) {
            log.info("Delete " + numberUserIllegal + " user illegal !");
        }

    }
}
