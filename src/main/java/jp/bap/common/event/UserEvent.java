package jp.bap.common.event;

import jp.bap.common.dto.UserPrincipalDto;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class UserEvent extends ApplicationEvent {

    private UserPrincipalDto userPrincipalDto;

    public UserEvent(Object source, UserPrincipalDto userPrincipalDto) {
        super(source);
        this.userPrincipalDto = userPrincipalDto;
    }

}
