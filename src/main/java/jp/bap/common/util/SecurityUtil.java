package jp.bap.common.util;

import jp.bap.common.EMEnum;
import jp.bap.common.dto.UserPrincipalDto;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Objects;
import java.util.stream.Collectors;

public class SecurityUtil {

    public static UserPrincipalDto getUser() {
        if(!((SecurityContextHolder.getContext().getAuthentication()) instanceof AnonymousAuthenticationToken)) {
            UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken)
                    SecurityContextHolder.getContext().getAuthentication();
            if (authentication.getPrincipal() instanceof UserPrincipalDto) {
                return (UserPrincipalDto) authentication.getPrincipal();
            }
        }
        else
            throw new jp.bap.common.exception.AuthenticationException(MessageConstants.NOT_AUTHENTICATED);
        return null;
    }
    public static EMEnum.RoleName getRoleName(){
        EMEnum.RoleName roleName;
        try {
            String role =  Objects.requireNonNull(getUser()).getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining());
            roleName=EMEnum.RoleName.valueOf(role);
        }catch (IllegalArgumentException e){
            throw new BadRequestException(MessageConstants.ROLE_NOT_FOUND);
        }
        return roleName;

    }
}
