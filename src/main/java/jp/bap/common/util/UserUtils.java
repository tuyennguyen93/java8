package jp.bap.common.util;

import jp.bap.common.dto.SignUpRequestDto;
import jp.bap.common.dto.ErrorDto;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserUtils {
    @Autowired
    UserRepository userRepository;
//    public Map<String, List<ErrorDto>> checkUser(SignUpRequestDto user){
//
//        Map<String, List<ErrorDto>> userDetailError = new HashMap<>();
//
//        List<ErrorDto> allError = new ArrayList<>();
//        List<String> errorEmail = new ArrayList<>();
//        List<String> errorPassword = new ArrayList<>();
//        List<String> errorPhone = new ArrayList<>();
//        List<String> errorDob = new ArrayList<>();
//        List<String> errorFullName = new ArrayList<>();
//        ErrorDto errorDto;
//
//        /* EMAIL */
//        if(user.getEmail() != null){
//            if(!isValidEmail(user.getEmail())){
//                errorEmail.add(MessageConstants.EMAIL_INVALIDATE);
//            }
//            if(userRepository.existsByEmail(user.getEmail())){
//                errorEmail.add(MessageConstants.EMAIL_ALREADY_EXISTS);
//            }
//        }else {
//            errorEmail.add(MessageConstants.EMAIL_NULL);
//        }
//        if(!errorEmail.isEmpty()){
//            errorDto = new ErrorDto(user.getEmail(),"Email",errorEmail);
//            allError.add(errorDto);
//        }
//
//        /* PASSWORD */
//        if(user.getPassword() != null && user.getPassword().length()<APIConstants.PASSWORD_LENGTH){
//            errorPassword.add(MessageConstants.PASSW_LENGTH);
//        }else if(user.getPassword() == null){
//            errorPassword.add(MessageConstants.PASSW_NULL);
//        }
//        if(!errorPassword.isEmpty()){
//            errorDto = new ErrorDto(user.getPassword(),"Password",errorPassword);
//            allError.add(errorDto);
//        }
//
//        /* DOB */
//        if(user.getDob() == null){
//            errorDob.add(MessageConstants.DOB_NOT_CORRECT);
//            errorDto = new ErrorDto(user.getDob(),"Dob",errorDob);
//            allError.add(errorDto);
//        }
//        /* PHONE NUMBER */
//        if(user.getPhoneNumber() != null && !isValidPhone(user.getPhoneNumber())){
//            errorPhone.add(MessageConstants.INVALID_PHONENUMBER);
//        }else if(user.getPhoneNumber() == null){
//            errorPhone.add(MessageConstants.PHONE_NULL);
//        }
//        if(!errorPhone.isEmpty()){
//            errorDto = new ErrorDto(user.getPhoneNumber(),"Phone Number",errorPhone);
//            allError.add(errorDto);
//        }
//        /* FULL NAME */
//        if(user.getFullName() != null && !isValidFullName(user.getFullName())){
//            errorFullName.add(MessageConstants.INVALID_FULLNAME);
//        }else if(user.getFullName() == null){
//            errorFullName.add(MessageConstants.FULLNAME_NULL);
//        }
//        if(!errorFullName.isEmpty()){
//            errorDto = new ErrorDto(user.getFullName(),"Full Name",errorFullName);
//            allError.add(errorDto);
//        }
//        if(!allError.isEmpty()){
//            userDetailError.put(user.getEmail(),allError);
//        }
//        return userDetailError;
//    }
public ErrorDto validationUser(SignUpRequestDto user){
    List<String> errors = new ArrayList<>();
    Map<String, List<ErrorDto>> userDetailError = new HashMap<>();


    ErrorDto errorDto;

    /* EMAIL */
    if(user.getEmail() != null){
        if(!isValidEmail(user.getEmail())){
            errors.add(MessageConstants.EMAIL_INVALIDATE);
        }
        if(userRepository.existsByEmail(user.getEmail())){
            errors.add(MessageConstants.EMAIL_ALREADY_EXISTS);
        }
    }else {
        errors.add(MessageConstants.EMAIL_NULL);
    }


    /* PASSWORD */
    if(user.getPassword() != null && user.getPassword().length()<APIConstants.PASSWORD_LENGTH){
        errors.add(MessageConstants.PASSW_LENGTH);
    }else if(user.getPassword() == null){
        errors.add(MessageConstants.PASSW_NULL);
    }


    /* DOB */
    if(user.getDob() == null){
        errors.add(MessageConstants.DOB_NOT_CORRECT);

    }
    /* PHONE NUMBER */
    if(user.getPhoneNumber() != null && !isValidPhone(user.getPhoneNumber())){
        errors.add(MessageConstants.INVALID_PHONENUMBER);
    }else if(user.getPhoneNumber() == null){
        errors.add(MessageConstants.PHONE_NULL);
    }

    /* FULL NAME */
    if(user.getFullName() != null && !isValidFullName(user.getFullName())){
        errors.add(MessageConstants.INVALID_FULLNAME);
    }else if(user.getFullName() == null){
        errors.add(MessageConstants.FULLNAME_NULL);
    }

    errorDto = !errors.isEmpty()?(new ErrorDto(user.getEmail(),errors)):new ErrorDto();

    return errorDto;
}
    public static boolean isValidEmail(String email)
    {
        String regex = APIConstants.REGEX_EMAIL;
        return email.matches(regex);
    }
    public static boolean isValidPhone(String number)
    {
        String regex = APIConstants.REGEX_PHONE;
        return number.matches(regex);
    }
    public static boolean isValidFullName(String fullname)
    {
        String regex = APIConstants.REGEX_FULLNAME;
        return fullname.matches(regex);
    }
    public static boolean isDateValid(String date)
    {
        try {
            SimpleDateFormat df = new SimpleDateFormat(APIConstants.DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }
}
