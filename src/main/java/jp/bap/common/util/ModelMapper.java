package jp.bap.common.util;


import jp.bap.common.dto.UserPrincipalDto;
import jp.bap.common.dto.UserProfileDto;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Level;
import jp.bap.common.model.Role;
import jp.bap.common.model.User;
import org.springframework.beans.BeanUtils;

public class ModelMapper {
    public static UserProfileDto mapToUserProfile(Role role, Level level, Employee employee, User user){
        UserProfileDto userProfileDto = new UserProfileDto();
        BeanUtils.copyProperties(user,userProfileDto);
        BeanUtils.copyProperties(role , userProfileDto);
        BeanUtils.copyProperties(level , userProfileDto);
        BeanUtils.copyProperties(employee , userProfileDto);
        return userProfileDto;
    }
}
