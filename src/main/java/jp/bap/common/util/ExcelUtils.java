package jp.bap.common.util;


import com.csvreader.CsvReader;
import jp.bap.common.dto.SignUpRequestDto;

import jp.bap.common.model.Resource;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.Constants;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ExcelUtils {
    public static Workbook getWorkbook(MultipartFile mf) throws IOException {
        String filePath = mf.getOriginalFilename();
        if (!validateExcel(filePath)) {
            throw new FileNotFoundException(MessageConstants.INCORRECT_FILE);
        }
        InputStream inputStream = mf.getInputStream();
        Workbook workbook = null;
        assert filePath != null;
        if (isExcel2003(filePath)) {
            workbook = new HSSFWorkbook(inputStream);
        } else if (isExcel2007(filePath)) {
            workbook = new XSSFWorkbook(inputStream);
        } else if (isCSV(filePath)) {
            workbook = readCsv(inputStream);
        }
        return workbook;
    }

    /* Excel ver 2003 */
    public static Boolean isExcel2003(String filePath) {
        return filePath.matches("^.+\\.(?i)(xls)$");
    }

    /* Excel ver 2007 over */
    public static Boolean isExcel2007(String filePath) {
        return filePath.matches("^.+\\.(?i)(xlsx)$");
    }

    /* CSV FILE */
    public static Boolean isCSV(String filePath) {
        return filePath.matches("^.+\\.(?i)(csv)$");
    }

    public static Boolean validateExcel(String filePath) {
        if (filePath == null || !(isExcel2003(filePath) || isExcel2007(filePath) || isCSV(filePath))) {
            return false;
        }
        return true;
    }

    public static List<SignUpRequestDto> readContent(MultipartFile file) throws IOException {
        List<SignUpRequestDto> dataList = new ArrayList<>();
        Workbook wb = getWorkbook(file);
        Sheet sheet = wb.getSheetAt(0);

        int rowNum = sheet.getLastRowNum();
        Row row;
        for (int i = 1; i <= rowNum; i++) {
            row = sheet.getRow(i);
            int col = 0;
            SignUpRequestDto signUpRequest = new SignUpRequestDto();
            signUpRequest.setFullName(getStringValue(row.getCell(col++)));
            signUpRequest.setEmail(getStringValue(row.getCell(col++)));
            signUpRequest.setPassword(getStringValue(row.getCell(col++)));
            signUpRequest.setPhoneNumber(getStringValue(row.getCell(col++)));
            LocalDate date;
            try {
                date = LocalDate.parse(getStringValue(row.getCell(col++)));
            } catch (DateTimeParseException ex) {
                date = null;
            }
            signUpRequest.setDob(date);
            dataList.add(signUpRequest);
        }
        return dataList;
    }

    public static String getStringValue(Cell cell) {
        String value = "";
        switch (cell.getCellType()) {
            case NUMERIC:
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    value = sdf.format(HSSFDateUtil.getJavaDate(cell.getNumericCellValue()));
                    break;
                } else {
                    value = new DecimalFormat("0").format(cell.getNumericCellValue());
                }
                break;
            case STRING:
                value = cell.getStringCellValue();
                break;
            case BOOLEAN:
                value = cell.getBooleanCellValue() + StringUtils.EMPTY;
                break;
            case FORMULA: // dạng công thức
                value = cell.getCellFormula() + StringUtils.EMPTY;
                break;
            case BLANK: // Trống
                value = StringUtils.EMPTY;
                break;
            case ERROR:
                value = StringUtils.EMPTY;
                break;
            default:
                value = StringUtils.EMPTY;
                break;
        }
        return value;
    }

    public static Workbook readCsv(InputStream inputStream) throws IOException {
        XSSFWorkbook workBook = new XSSFWorkbook();
        ArrayList<String[]> dataList = new ArrayList<>();
        CsvReader reader = new CsvReader((inputStream), APIConstants.COMMA, Charset.forName(StandardCharsets.UTF_8.displayName()));
        XSSFSheet sheet = workBook.createSheet(APIConstants.SHEET_PAGE);
        try {
            while (reader.readRecord()) {
                dataList.add(reader.getValues());
            }
            reader.close();

            for (int rowNum = 0; rowNum < dataList.size(); rowNum++) {
                String[] data = dataList.get(rowNum);
                XSSFRow row = sheet.createRow(rowNum);
                for (int columnNum = 0; columnNum < data.length; columnNum++) {
                    XSSFCell cell = row.createCell(columnNum);
                    cell.setCellValue(data[columnNum]);
                }
            }

            return workBook;
        } catch (Exception ex) {
            /* Lỗi trong khi đang covert csv thành excel */
            ex.printStackTrace();

        } finally {
            if (Objects.nonNull(workBook)) {
                workBook.close();
            }
        }
        return workBook;
    }

    public static boolean isValidEmail(String email) {
        String regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
        return email.matches(regex);
    }

    final static String DATE_FORMAT = "dd-MM-yyyy";

    public static boolean isDateValid(String date) {
        try {
            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
            df.setLenient(false);
            df.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * Export  xlsx.
     *
     * @return ResponseEntity<Resource>
     * @throws IOException {@link IOException}
     */
    public static InputStreamResource getfileXLSX(String fileName) throws IOException {
        Workbook wb = new XSSFWorkbook();
        //Create sheet
        Sheet sheet = wb.createSheet("abc");
        int i = 0;
        // Create header row
        Row row = sheet.createRow(i);
        // no. column

        row.createCell(0).setCellValue("FullName");

        row.createCell(1).setCellValue("Email");

        row.createCell(2).setCellValue("Password");

        row.createCell(3).setCellValue("PhoneNumber");

        row.createCell(4).setCellValue("dob");


        //set file name
        String FullfileName = fileName + System.currentTimeMillis() + ".xlsx";
        //set location to write
        File file = new File( System.getProperty("java.io.tmpdir")+ "/" +FullfileName);

        // Write the output to a file
        FileOutputStream fileOut = new FileOutputStream(file);
        wb.write(fileOut);
        fileOut.close();
        //set file to read and export
        InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
        return resource;
    }


}
