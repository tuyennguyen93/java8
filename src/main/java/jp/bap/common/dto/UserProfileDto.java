package jp.bap.common.dto;

import jp.bap.common.model.Level;
import jp.bap.common.model.Role;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserProfileDto {
    private Integer id;
    private String email;
    private String fullName;
    private LocalDate dob;
    private boolean isActive;
    private LocalDate joinDate;
    private Role role;
    private String phoneNumber;
    private Level level;
    private LocalDateTime created;
}
