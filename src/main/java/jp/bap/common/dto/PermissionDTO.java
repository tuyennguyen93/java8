package jp.bap.common.dto;

import jp.bap.common.EMEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class PermissionDTO {

    @NotNull
    private Integer rolePermissionId;

    @NotNull
    private EMEnum.ResourceStatus status;

    @AssertTrue(message = "PermissionStatus must be in format: NON || CAN_VIEW || CAN_EDIT")
    private boolean isStatus(){
        if(status != null)
            return EMEnum.ResourceStatus.contains(status.name());
        return false;
    }
}
