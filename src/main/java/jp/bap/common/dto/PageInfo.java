package jp.bap.common.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
public class PageInfo<T> implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private long total;
    private int pageSize;
    private int totalPage;
    private int page;
        private List<T> content;

    public PageInfo() {
    }

    public void setPage(int page) {
        this.page = page + 1;
    }

    public static <T> PageInfo<T> pagingResponse(Page<T> page) {
        // page info
        PageInfo<T> pageInfo = new PageInfo<T>();
        pageInfo.setTotal(page.getTotalElements());
        pageInfo.setPageSize(page.getSize());
        pageInfo.setTotalPage(page.getTotalPages());
        pageInfo.setPage(page.getNumber()+1);
        pageInfo.setContent(page.getContent());

        return pageInfo;
    }

}
