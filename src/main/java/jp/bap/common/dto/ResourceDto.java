package jp.bap.common.dto;

import jp.bap.common.EMEnum;
import lombok.Data;

@Data
public class ResourceDto {

    private EMEnum.RoleName nameRole;
    private boolean canEdit;
    private boolean canView;
    private EMEnum.ResourceName nameResource;

    public ResourceDto(EMEnum.RoleName nameRole, boolean canEdit, boolean canView, EMEnum.ResourceName nameResource) {
        this.nameRole = nameRole;
        this.canEdit = canEdit;
        this.canView = canView;
        this.nameResource = nameResource;
    }
}
