package jp.bap.common.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class UserImportDto {
    private List<SignUpRequestDto> success;
    private List<Object> fails;
    private Integer total;
}
