package jp.bap.common.dto;

import jp.bap.common.EMEnum;
import jp.bap.common.model.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.BeanUtils;

@Getter
@Setter
public class UserDTO extends BaseDTO {

    private String email;

    private String status;

    private Integer employeeId;

    private String employeeName;

    private String roleName;

    private String level = "";

    public UserDTO() {

    }

    public UserDTO(User user) {
        super();
        BeanUtils.copyProperties(user, this);
        this.roleName = user.getRole().getName().name();
        this.employeeName = user.getEmployee().getFullName();
        this.status = user.isActive() ? EMEnum.UserStatus.ACTIVE.getDisplay() : EMEnum.UserStatus.INACTIVE.getDisplay();
        if (user.getEmployee().getLevel() != null) {
            this.level = user.getEmployee().getLevel().getName();
        }
    }
}