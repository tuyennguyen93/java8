package jp.bap.common.dto;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@MappedSuperclass
public abstract class BaseDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    protected Integer id;

    protected LocalDateTime created;

    protected LocalDateTime modified;

}
