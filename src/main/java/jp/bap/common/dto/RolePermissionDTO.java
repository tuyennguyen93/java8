package jp.bap.common.dto;

import jp.bap.common.EMEnum;
import jp.bap.common.model.RolePermission;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class RolePermissionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private String resourceName;

    private EMEnum.ResourceStatus status;

    public RolePermissionDTO(RolePermission permission) {

        this.id = permission.getId();

        this.resourceName = permission.getResource().getName().name();

        if (permission.getCanView()) {
            if (permission.getCanEdit()) {
                this.status = EMEnum.ResourceStatus.CAN_EDIT;
            } else {
                this.status = EMEnum.ResourceStatus.CAN_VIEW;
            }
        } else {
            this.status = EMEnum.ResourceStatus.NON;
        }

    }
}
