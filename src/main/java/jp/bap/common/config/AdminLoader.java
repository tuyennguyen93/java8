package jp.bap.common.config;

import jp.bap.common.EMEnum;
import jp.bap.common.aspect.LoggingAspect;
import jp.bap.common.model.Employee;
import jp.bap.common.model.Role;
import jp.bap.common.model.User;
import jp.bap.common.repository.EmployeeRepository;
import jp.bap.common.repository.RoleRepository;
import jp.bap.common.repository.UserRepository;
import jp.bap.common.util.constant.APIConstants;
import jp.bap.common.util.constant.MessageConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContextException;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
@Order(2)
public class AdminLoader implements CommandLineRunner {

    private final Logger log = LoggerFactory.getLogger(LoggingAspect.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Value("${admin.email}")
    private String email;

    @Value("${admin.password}")
    private String password;

    @Value("${admin.name}")
    private String name;

    @Value("${admin.phone}")
    private String phone;

    @Value("${admin.dob}")
    private String dob;

    @Override
    public void run(String... args) throws Exception {
        if (!userRepository.existsByEmail(email)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(APIConstants.FORMATTER);
            LocalDate timeJoin = LocalDate.now();
            User user = new User();
            user.setEmail(email);
            user.setPassword(passwordEncoder.encode(password));
            Role userRole = roleRepository.findByName(EMEnum.RoleName.ROLE_ADMIN)
                    .orElseThrow(() -> new ApplicationContextException(MessageConstants.ROLE_ALREADY_EXISTS));
            user.setRoleId(userRole.getId());

            Employee employee = new Employee();
            employee.setDob(timeJoin.parse(dob, formatter));
            employee.setFullName(name);
            employee.setPhoneNumber(phone);
            employee.setJoinDate(timeJoin);
            Employee resultEmployee = employeeRepository.save(employee);
            user.setEmployeeId(resultEmployee.getId());
            userRepository.save(user);

            log.warn(MessageConstants.ADMIN_INITIALIZATION);
        }
    }

}
