package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.PositiveOrZero;

@Entity
@Data
@Table(name = "report_detail")
public class ReportDetail extends BaseModel {

    @Column(name = "comment")
    private String comment;

    @Max(10)
    @PositiveOrZero
    @Column(name = "evaluation")
    private Double evaluation;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "report_id", updatable = false, insertable = false)
    private Report report;

    @Column(name = "report_id")
    private Integer reportId;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "criteria_id", updatable = false, insertable = false)
    private Criteria criteria;

    @Column(name = "criteria_id")
    private Integer criteriaId;
}
