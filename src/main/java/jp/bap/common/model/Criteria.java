package jp.bap.common.model;


import jp.bap.common.base.BaseModel;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "criteria")
public class Criteria extends BaseModel {

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "report_type")
    private String reportType;

    @OneToMany(mappedBy = "criteria")
    private List<ReportDetail> reportDetails = new ArrayList<>();

}
