package jp.bap.common.model;


import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "role")
@Data
public class Role extends BaseModel {

    @Column(name = "name")
    @Enumerated(EnumType.STRING)
    private EMEnum.RoleName name;

    public Role() {
        super();
    }

    public Role(EMEnum.RoleName roleName) {
        this.name = roleName;
    }

    //
//    @OneToMany(mappedBy = "role")
//    private List<RolePermission> rolePermissions = new ArrayList<>();
//
//    @OneToMany(mappedBy = "role")
//    private List<User> users = new ArrayList<>();
}
