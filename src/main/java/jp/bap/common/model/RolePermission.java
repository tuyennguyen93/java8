package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Data
@Table(name = "role_permission")
public class RolePermission extends BaseModel {

    @Column(name = "can_view")
    private Boolean canView;

    @Column(name = "can_edit")
    private Boolean canEdit;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "role_id", updatable = false, insertable = false)
    private Role role;

    @Column(name = "role_id")
    private Integer roleId;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "resource_id", updatable = false, insertable = false)
    private Resource resource;

    @Column(name = "resource_id")
    private Integer resourceId;

    public RolePermission(){
        super();
    }

    public RolePermission(Boolean canView, Boolean canEdit, Role role, Integer roleId, Resource resource, Integer resourceId) {
        this.canView = canView;
        this.canEdit = canEdit;
        this.role = role;
        this.roleId = roleId;
        this.resource = resource;
        this.resourceId = resourceId;
    }
}
