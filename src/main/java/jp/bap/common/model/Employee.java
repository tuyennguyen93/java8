package jp.bap.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jp.bap.common.base.BaseModel;
import jp.bap.common.validation.ContactNumberConstraint;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.*;

@Entity
@Data
@Table(name = "employee")
public class Employee extends BaseModel {

    @Size(max = 5)
    @NotEmpty
    @Column(name = "full_name")
    private String fullName;

    @Past(message = "Date of birth greater than now day")
    @Column(name = "dob")
    private LocalDate dob;

    @Column(name = "join_date")
    private LocalDate joinDate;

    @Column(name = "phone_number")
    private String phoneNumber;

    @OneToOne(mappedBy = "employee" ,fetch = FetchType.LAZY)
    @JsonIgnore
    private User user;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "level_id", updatable = false, insertable = false)
    private Level level;

    @Column(name = "level_id")
    private Integer levelId;

    @Column(name = "salary_present")
    private Double salaryPresent;

//    @OneToMany(mappedBy = "employee")
//    private List<Report> reports =  new ArrayList<>();
//
    @OneToMany(mappedBy = "employee",fetch = FetchType.LAZY)
    private List<MemberProject> joinProjects = new ArrayList<>();

//    @OneToMany(mappedBy = "employee")
//    private List<ProjectLeader> projectLeaders = new ArrayList<>();
//
    @OneToMany(mappedBy = "projectManager",fetch = FetchType.LAZY)
    private List<Project> managerProjects = new ArrayList<>();


    @Column(name = "official_date")
    private LocalDate officialDate;

    @OneToMany(mappedBy = "employee") // MapopedBy trỏ tới tên biến employee ở trong Salary.
    private List<Salary> salary = new ArrayList<>();

    @OneToMany(mappedBy = "employee",fetch = FetchType.LAZY)
    private List<Technology> technology = new ArrayList<>();

}
