package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@Table(name = "member_project")
public class MemberProject extends BaseModel {

    @Column(name = "join_on_date")
    private LocalDate joinOnDate;

    @Column(name = "get_out_date")
    private LocalDate getOutDate;

    @Column(name = "member_type")
    private String memberType;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "employee_id", updatable = false, insertable = false)
    private Employee employee;

    @Column(name = "employee_id")
    private Integer employeeId;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "project_id", updatable = false, insertable = false)
    private Project project;

    @Column(name = "project_id")
    private Integer projectId;
}
