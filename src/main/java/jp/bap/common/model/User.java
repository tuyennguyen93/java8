package jp.bap.common.model;

import jp.bap.common.base.BaseModel;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@AllArgsConstructor
@Table(name = "user")
public class User extends BaseModel {

    @Email
    private String email;

    @NotNull
    private String password;

    @Column(name = "is_active")
    private boolean isActive=true;

    @NotFound(action = NotFoundAction.IGNORE)
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", updatable = false, insertable = false)
    private Employee employee;

    @Column(name = "employee_id")
    private Integer employeeId;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id", updatable = false, insertable = false)
    private Role role;

    @Column(name = "role_id")
    private Integer roleId;

    public User() {
        super();
    }
}
