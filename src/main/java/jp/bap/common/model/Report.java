package jp.bap.common.model;

import jp.bap.common.EMEnum;
import jp.bap.common.base.BaseModel;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "report")
public class Report extends BaseModel {

    @Column(name = "name")
    private String name;

    @Column(name = "due_date")
    private LocalDate dueDate;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private EMEnum.ReportStatus status;

    @Column(name = "report_to_id")
    private Integer reportToId;

    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "project_id", updatable = false, insertable = false)
    private Project project;

    @Column(name = "project_id")
    private Integer projectId;


    @NotFound(action = NotFoundAction.IGNORE)
    @ManyToOne
    @JoinColumn(name = "employee_id", updatable = false, insertable = false)
    private Employee employee;

    @Column(name = "employee_id")
    private Integer employeeId;

    @OneToMany(mappedBy = "report")
    private List<ReportDetail> reportDetails = new ArrayList<>();
}
