package jp.bap.common.base;

import jp.bap.common.EMEnum;
import jp.bap.common.dto.PageInfo;
import jp.bap.common.dto.UserPrincipalDto;
import jp.bap.common.exception.BadRequestException;
import jp.bap.common.model.User;
import jp.bap.common.util.constant.MessageConstants;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.stream.Collectors;

/*
* Base Service
*
*/
@Service
public class BaseService {


    @Value("${app.paging.default.limit}")
    protected int pagingDefaultLimit;

    @Value("${app.paging.default.pageInit}")
    protected int pagingDefaultPageInit;

    @Value("${app.paging.default.offset}")
    protected int pagingDefaultOffset;


    /**
     * Build {@link PageRequest} with page and limit
     *
     * @param page
     * @param limit
     * @return
     */
    protected PageRequest buildPageRequest(Integer page, Integer limit) {
        page = page == null ? 0 : page - this.pagingDefaultPageInit;
        limit = limit == null ? this.pagingDefaultLimit : limit;
        return PageRequest.of(page, limit);
    }

    /**
     * Build {@link PageRequest} with page, limit and sort
     *
     * @param page
     * @param limit
     * @param sort
     * @return
     */
    protected PageRequest buildPageRequest(Integer page, Integer limit, Sort sort) {
        page = page == null ? 0 : page - this.pagingDefaultPageInit;
        limit = limit == null ? this.pagingDefaultLimit : limit;
        return PageRequest.of(page, limit, sort);
    }

    /**
     * Get current user has logged.
     *
     * @return
     */
    protected  UserPrincipalDto getCurrentUser() {
        if(!((SecurityContextHolder.getContext().getAuthentication()) instanceof AnonymousAuthenticationToken)) {
            UsernamePasswordAuthenticationToken authentication = (UsernamePasswordAuthenticationToken)
                    SecurityContextHolder.getContext().getAuthentication();
            if (authentication.getPrincipal() instanceof UserPrincipalDto) {
                return (UserPrincipalDto) authentication.getPrincipal();
            }
        }
        else
            throw new jp.bap.common.exception.AuthenticationException(MessageConstants.NOT_AUTHENTICATED);
        return null;
    }

    /**
     * Get current role name
     *
     * @return
     */
    protected  EMEnum.RoleName getCurrentRoleName(){
        EMEnum.RoleName roleName;
        try {
            String role =  Objects.requireNonNull(getCurrentUser()).getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining());
            roleName=EMEnum.RoleName.valueOf(role);
        }catch (IllegalArgumentException e){
            throw new BadRequestException(MessageConstants.ROLE_NOT_FOUND);
        }
        return roleName;

    }

    /**
     * Create paging with basic information
     *
     * @param <T>
     * @param page {@link Page} of T
     * @return {@link PageInfo}
     */
    protected <T> PageInfo<T> pagingResponse(Page<T> page) {
        // page info
        PageInfo<T> pageInfo = new PageInfo<T>();
        pageInfo.setTotal(page.getTotalElements());
        pageInfo.setPageSize(page.getSize());
        pageInfo.setTotalPage(page.getTotalPages());
        pageInfo.setPage(page.getNumber());
        pageInfo.setContent(page.getContent());

        return pageInfo;
    }

}
