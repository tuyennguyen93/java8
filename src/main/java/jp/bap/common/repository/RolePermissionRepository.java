package jp.bap.common.repository;

import jp.bap.common.EMEnum;
import jp.bap.common.model.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, Integer> {

    @Query("SELECT r FROM RolePermission r where r.roleId =:roleId")
    List<RolePermission> getByRoleId(@Param("roleId") Integer roleId);

    List<RolePermission> getByRoleName(EMEnum.RoleName roleName);
}
