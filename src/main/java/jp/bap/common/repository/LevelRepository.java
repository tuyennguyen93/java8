package jp.bap.common.repository;


import jp.bap.common.model.Level;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LevelRepository extends JpaRepository<Level,Integer> {
}
