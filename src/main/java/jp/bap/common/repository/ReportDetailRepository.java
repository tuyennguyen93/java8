package jp.bap.common.repository;

import jp.bap.common.model.ReportDetail;
import jp.bap.common.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReportDetailRepository extends JpaRepository<ReportDetail, Integer> {
    @Query("SELECT r FROM ReportDetail r WHERE r.reportId= :reportId")
    List<ReportDetail> findByReportId(@Param("reportId") Integer reportId);
}
