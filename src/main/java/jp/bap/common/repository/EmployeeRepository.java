package jp.bap.common.repository;

import jp.bap.common.model.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Query("select e from Employee e where :name is null or :name like '' or e.fullName  like %:name% ")
    Page<Employee> searchEmployee(@Param("name") String name, Pageable pageable);
}
