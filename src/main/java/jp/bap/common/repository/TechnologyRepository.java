package jp.bap.common.repository;

import jp.bap.common.EMEnum;
import jp.bap.common.dto.ResourceDto;
import jp.bap.common.model.Role;
import jp.bap.common.model.Technology;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TechnologyRepository extends JpaRepository<Technology,Integer> {

    @Query("SELECT t FROM Technology t where t.employee.id =:idEmploye")
    List<Technology> findAllByIdEmploye(@Param("idEmploye") Integer idEmploye);
}
