package jp.bap.common.repository;

import jp.bap.common.model.Criteria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CriteriaRepository extends JpaRepository<Criteria, Integer> {

    List<Criteria> findAllByReportType(String reportType);
}
