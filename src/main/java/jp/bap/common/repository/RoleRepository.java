package jp.bap.common.repository;

import jp.bap.common.EMEnum;
import jp.bap.common.dto.ResourceDto;
import jp.bap.common.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role,Integer> {
    @Query("SELECT r FROM Role r where r.name =:roleName")
    Optional<Role> findByName(@Param("roleName") EMEnum.RoleName roleName);

    @Query("select r2.name,rp.canEdit,rp.canView " +
            "from Role r " +
            "join RolePermission rp on r.id = rp.id " +
            "join Resource r2 on rp.id = r2.id " +
            "where r.name=:roleName")
    List<ResourceDto> findResourceByNameRole(@Param("roleName") EMEnum.RoleName roleName);

    @Query("select new jp.bap.common.dto.ResourceDto(r.name,rp.canEdit,rp.canView,r2.name) " +
            "from Role r " +
            "join RolePermission rp on r.id = rp.roleId " +
            "join Resource r2 on rp.id = r2.id " +
            "where r2.name =:resourceName AND r.name =:roleName")
    Optional<ResourceDto> findByRoleAndResource(@Param("resourceName") EMEnum.ResourceName resourceName, @Param("roleName") EMEnum.RoleName roleName);
}
