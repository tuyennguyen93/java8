package jp.bap.common.repository;

import jp.bap.common.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    @Query("SELECT u FROM User u where u.email=:email")
    Optional<User> findByEmail(@Param("email") String email);

    List<User> findByIdIn(List<Long> userIds);

    Boolean existsByEmail(String email);

    Page<User> findAll(Pageable pageable);


    @Modifying(clearAutomatically = true)
    @Transactional
    @Query(value = "UPDATE user SET role_id = '5' WHERE  email <>:email AND role_id = '1';", nativeQuery = true)
    int updateAllUserIllegal(@Param("email") String email);

    @Modifying(clearAutomatically = true)
    @Transactional
    @Query("UPDATE User u set u.password = :password where u.id = :id")
    void updatePassword(@Param("password") String password, @Param("id") Integer id);
}
