package jp.bap.common.repository;

import jp.bap.common.model.MemberProject;
import jp.bap.common.model.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface MemberProjectRepository extends JpaRepository<MemberProject, Integer> {


    List<MemberProject> findByProjectId(Integer projectId);

    List<MemberProject> findByProjectIdAndGetOutDate(Integer projectId, LocalDate getOutDate);

    MemberProject findByEmployeeIdAndProjectId(Integer employeeId, Integer projectId);

    MemberProject findByEmployeeIdAndProjectIdAndGetOutDate(Integer employeeId, Integer projectId, LocalDate getOutDate);

    Boolean existsByEmployeeIdAndProjectIdAndGetOutDate(Integer employeeId, Integer projectId, LocalDate getOutDate);

    Boolean existsByProjectIdAndGetOutDate(Integer projectId, LocalDate getOutDate);

    /*List Member with getOutDate = null (members are joining project)*/
    List<MemberProject> findAllByProjectIdAndMemberTypeAndGetOutDate(Integer projectId, String memberType, LocalDate getOutDate);

    /*List Member with getOutDate = null (members are joining project)*/
    List<MemberProject> findAllByEmployeeIdAndMemberTypeAndGetOutDate(Integer employeeId, String memberType, LocalDate getOutDate);

    /*List Member with getOutDate = null (members are joining project)*/
    List<MemberProject> findAllByEmployeeIdAndGetOutDate(Integer employeeId, LocalDate getOutDate);

    Boolean existsByProjectIdAndEmployeeIdAndMemberTypeAndGetOutDate(Integer projectId, Integer employeeId, String memberType, LocalDate getOutDate);
}
