package jp.bap.common.repository;

import jp.bap.common.EMEnum;
import jp.bap.common.model.Report;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
    Boolean existsByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus(String name,
                                                                      Integer projectId,
                                                                      Integer employeeId,
                                                                      Integer reportToId,
                                                                      EMEnum.ReportStatus status);
    Report findByNameAndProjectIdAndEmployeeIdAndReportToIdAndStatus(String name,
                                                                     Integer projectId,
                                                                     Integer employeeId,
                                                                     Integer reportToId,
                                                                     EMEnum.ReportStatus status);

    /*get Report Not Admin */
    @Query("SELECT r FROM Report r WHERE r.employeeId = :EmId")
    Page<Report>getReportByNotAdmin(@Param("EmId") Integer EmId, Pageable pageable);

    /*get Report By Status Not Admin */
    @Query("SELECT r FROM Report r WHERE r.employeeId = :EmId AND r.status =:status")
    Page<Report>getReportStatusByNotAdmin(@Param("EmId") Integer EmId, Pageable pageable,
                                          @Param("status") EMEnum.ReportStatus status);

    /*get ReportDetails*/
    @Query("SELECT r FROM Report r WHERE r.id = :id ")
    Report getReportDetails(@Param("id") Integer id);

    /*get Report by EmployeeId */
    @Query("SELECT r FROM Report r WHERE r.employeeId = :EmId")
    Page<Report>getReportByEmId(@Param("EmId") Integer EmId, Pageable pageable);

    /* search custom by Admin */
    @Query("SELECT r FROM Report r "
            +"JOIN r.employee e "
            +"JOIN e.user u "
            +"JOIN r.project p "
            +"WHERE (:fullName IS NULL OR :fullName = '' OR e.fullName LIKE CONCAT('%',:fullName,'%'))"
            + "AND (:email IS NULL OR :email = '' OR u.email LIKE CONCAT('%',:email,'%'))"
            +" AND (:projectName IS NULL OR :projectName = '' OR p.name LIKE CONCAT('%',:projectName,'%'))")
    Page<Report>findReportCustomByAdmin(@Param("fullName") String fullName,
                                        @Param("email") String email,
                                        @Param("projectName") String projectName,
                                        Pageable pageable);

    /* search custom by NotAdmin */
    @Query("SELECT r FROM Report r "
            +"JOIN r.employee e "
            +"JOIN e.user u "
            +"JOIN r.project p "
            +"WHERE (:fullName IS NULL OR :fullName = '' OR e.fullName LIKE CONCAT('%',:fullName,'%'))"
            + "AND (:email IS NULL OR :email = '' OR u.email LIKE CONCAT('%',:email,'%'))"
            +" AND (:projectName IS NULL OR :projectName = '' OR p.name LIKE CONCAT('%',:projectName,'%'))"
            +"AND r.employeeId =:emId ")
    Page<Report>findReportCustomByNotAdmin(@Param("fullName") String fullName,
                                           @Param("email") String email,
                                           @Param("projectName") String projectName,
                                           @Param("emId") Integer emId,
                                           Pageable pageable);

    Page<Report> findAllByStatus(EMEnum.ReportStatus status, Pageable pageable);
}
