package jp.bap.common.aspect;

import jp.bap.common.dto.ResourceDto;
import jp.bap.common.repository.RoleRepository;
import jp.bap.common.util.SecurityUtil;
import jp.bap.common.util.constant.MessageConstants;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.function.Function;

@Aspect
@Component
public class PermissionAspect {
    @Autowired
    RoleRepository roleRepository;

    @Around("execution(@jp.bap.common.aspect.PermissionCheck * *(..)) && @annotation(permissionCheck)")
    public Object PermissionCheckUser(ProceedingJoinPoint pjp, PermissionCheck permissionCheck) throws Throwable {
        if(!Objects.isNull(SecurityUtil.getRoleName())){
            ResourceDto resourceDto = roleRepository.findByRoleAndResource(permissionCheck.resource(), SecurityUtil.getRoleName())
                    .orElseThrow(() -> new AccessDeniedException(MessageConstants.ACCESS_DENIED));
            Function<ResourceDto,Boolean> permissionFunction = resourceDto1 -> {
                if (resourceDto1.isCanView() && permissionCheck.canView())
                    return true;
                if (resourceDto1.isCanEdit() && permissionCheck.canEdit())
                    return true;
                return false;
            };
            if(!permissionFunction.apply(resourceDto)){
                throw new AccessDeniedException(MessageConstants.ACCESS_DENIED);
            }
        }else {
            throw new AccessDeniedException(MessageConstants.ACCESS_DENIED);
        }
        return pjp.proceed();
    }
}
