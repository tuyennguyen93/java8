#
# salary TABLE
#
create table if not exists salary
(
    id			int auto_increment primary key,
    employee_id 		int not null,
    salary    double ,
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;

#
# technology TABLE
#
create table if not exists technology
(
    id			int auto_increment primary key,
    employee_id 		int not null,
    technology_name  varchar(150),
    created   		timestamp null,
    modified  		timestamp default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP
)charset=utf8;



ALTER TABLE employee
  ADD official_date datetime DEFAULT NULL;
